package view.web.bean.pedidos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.ItensPedido;
import basicas.negocio.ItensPedidoPK;
import basicas.negocio.Pedidos;
import basicas.negocio.Products;
import basicas.negocio.UnidadeMedida;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class EnviarXMLBean extends GenericBean<Pedidos> {

	private Products produto;
	private ItensPedido item;
	private UnidadeMedida unidMedida;
	private List<Products> listaProdutos;
	private List<CategoriaProduto> listCategorias;
	private List<UnidadeMedida> listUnidadeMedida;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setEnviarXML(false);

		basica = new Pedidos();
		unidMedida = new UnidadeMedida();
		produto = new Products();
		fachada = new Fachada();
		listaProdutos = fachada.listProdutosMaisVendidos();
		listCategorias = fachada.listCategorias();
		listUnidadeMedida = this.fachada.listUnidMedida();
	}

	public void salvarNotaXML() {
		try {
			if (basica == null) {
				ReturnMessage.setMessage("Aten��o! Envie uma nota.", "alert");
			} else {
				basica.setTipoNota(Pedidos.notaXML);
				fachada.salvarNotaXML(basica);
				basica = new Pedidos();
				ReturnMessage.setMessage("Nota enviado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao salvar nota XML! " + e.getMessage(), "criticalError");
		}
	}

	public void alterarProdXML() {
		if (item != null && unidMedida != null) {
			for (UnidadeMedida unid : listUnidadeMedida) {
				if (unid.getId().equals(unidMedida.getId())) {
					item.setUnidMedida(unid);
				}
			}
			RequestContext.getCurrentInstance().execute("PF('alterarItem').hide();");
		} else {
			ReturnMessage.setMessage("Erro ao selecionar o produto! ", "criticalError");
		}
	}

	public void selecionaProd(SelectEvent event) {
		if (item != null) {
			ItensPedidoPK chave = new ItensPedidoPK();
			chave.setProduto((Products) event.getObject());
			chave.setPedido(basica);
			item.setChaveComposta(chave);
		} else {
			ReturnMessage.setMessage("Erro ao selecionar o produto! ", "criticalError");
		}
	}

	public void xmlFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {

				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(event.getFile().getInputstream());
				Element element = doc.getDocumentElement();

				System.out.println("Vers�o da nota: " + element.getAttribute("versao"));

				basica = new Pedidos();
				DateFormat formatador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

				NodeList nivel1 = element.getChildNodes();

				for (int e = 0; e < nivel1.getLength(); e++) {
					if (nivel1.item(e).getNodeName().equals("NFe")) {
						NodeList nivel2 = nivel1.item(e).getChildNodes();

						for (int f = 0; f < nivel2.getLength(); f++) {
							if (nivel2.item(f).getNodeName().equals("infNFe")) {
								NodeList nivel3 = nivel2.item(f).getChildNodes();

								for (int g = 0; g < nivel3.getLength(); g++) {
									if (nivel3.item(g).getNodeName().equals("ide")) {
										NodeList nivel4C = nivel3.item(g).getChildNodes();

										for (int h = 0; h < nivel4C.getLength(); h++) {
											if (nivel4C.item(h).getNodeName().equals("dhEmi")) {

												System.out.println("Data Emi: " + nivel4C.item(h).getTextContent());
												Date dataPed = (Date) formatador
														.parse(nivel4C.item(h).getTextContent());
												basica.setDataPedido(dataPed);
											}

											if (nivel4C.item(h).getNodeName().equals("dhSaiEnt")) {

												System.out.println("Data Ent: " + nivel4C.item(h).getTextContent());
												Date dataEnt = (Date) formatador
														.parse(nivel4C.item(h).getTextContent());
												basica.setDataEntrega(dataEnt);
											}
										}
									}

									if (nivel3.item(g).getNodeName().equals("emit")) {
										NodeList nivel4A = nivel3.item(g).getChildNodes();

										for (int h = 0; h < nivel4A.getLength(); h++) {
											if (nivel4A.item(h).getNodeName().equals("xFant")) {

												System.out.println("Empresa: " + nivel4A.item(h).getTextContent());
												basica.getFornecedor().setEmpresaXML(nivel4A.item(h).getTextContent());
											}

											if (nivel4A.item(h).getNodeName().equals("enderEmit")) {
												NodeList nivel5A = nivel4A.item(h).getChildNodes();

												for (int i = 0; i < nivel5A.getLength(); i++) {
													if (nivel5A.item(i).getNodeName().equals("fone")) {

														System.out.println(
																"Fone: " + nivel5A.item(i).getTextContent() + "\n");

														basica.getFornecedor()
																.setFone(nivel5A.item(i).getTextContent());
														break;
													}
												}
											}
										}
									}

									if (nivel3.item(g).getNodeName().equals("det")) {
										NodeList nivel4B = nivel3.item(g).getChildNodes();

										for (int j = 0; j < nivel4B.getLength(); j++) {
											if (nivel4B.item(j).getNodeName().equals("prod")) {
												NodeList nivel5B = nivel4B.item(j).getChildNodes();
												item = new ItensPedido();

												for (int k = 0; k < nivel5B.getLength(); k++) {

													if (nivel5B.item(k).getNodeName().equals("cProd")) {

														System.out
																.println("C�digo: " + nivel5B.item(k).getTextContent());
														item.setCodProdXML(nivel5B.item(k).getTextContent());
													}
													if (nivel5B.item(k).getNodeName().equals("xProd")) {

														System.out.println(
																"Produto: " + nivel5B.item(k).getTextContent());
														item.setNomeProdXML(nivel5B.item(k).getTextContent());

													}
													if (nivel5B.item(k).getNodeName().equals("uCom")) {

														System.out.println(
																"Unidade: " + nivel5B.item(k).getTextContent());
														item.getUnidMedida()
																.setNomeXML(nivel5B.item(k).getTextContent());
													}
													if (nivel5B.item(k).getNodeName().equals("qCom")) {

														System.out.println(
																"Quantidade: " + nivel5B.item(k).getTextContent());
														item.setQtd(Float.parseFloat(nivel5B.item(k).getTextContent()));
													}
													if (nivel5B.item(k).getNodeName().equals("vUnCom")) {

														System.out.println(
																"Pre�o Unit: " + nivel5B.item(k).getTextContent());
														item.setPrecoUnit(
																Float.parseFloat(nivel5B.item(k).getTextContent()));
													}
													if (nivel5B.item(k).getNodeName().equals("vProd")) {

														System.out.println("Pre�o Total: "
																+ nivel5B.item(k).getTextContent() + "\n");
														item.setPrecoTotal(
																Float.parseFloat(nivel5B.item(k).getTextContent()));
													}
												}
												basica.getListaItensPedido().add(item);
												break;
											}
										}
									}

								}
							}
						}
					}
				}
				basica = fachada.verificarProdNotasXML(basica);
				ReturnMessage.setMessage("Nota XML enviada com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<String> getArrayListCategorias() {
		List<String> listaCategorias = new ArrayList<>();

		for (CategoriaProduto categoria : listCategorias) {
			listaCategorias.add(categoria.getNome());
		}
		return listaCategorias;
	}

	public Pedidos getBasica() {
		return basica;
	}

	public void setBasica(Pedidos basica) {
		this.basica = basica;
	}

	public List<Products> getListaProdutos() {
		return listaProdutos;
	}

	public void setListaProdutos(List<Products> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

	public List<CategoriaProduto> getListCategorias() {
		return listCategorias;
	}

	public void setListCategorias(List<CategoriaProduto> listCategorias) {
		this.listCategorias = listCategorias;
	}

	public List<UnidadeMedida> getListUnidadeMedida() {
		return listUnidadeMedida;
	}

	public void setListUnidadeMedida(List<UnidadeMedida> listUnidadeMedida) {
		this.listUnidadeMedida = listUnidadeMedida;
	}

	public Products getProduto() {
		return produto;
	}

	public void setProduto(Products produto) {
		this.produto = produto;
	}

	public UnidadeMedida getUnidMedida() {
		return unidMedida;
	}

	public void setUnidMedida(UnidadeMedida unidMedida) {
		this.unidMedida = unidMedida;
	}

	public ItensPedido getItem() {
		return item;
	}

	public void setItem(ItensPedido item) {
		this.item = item;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}