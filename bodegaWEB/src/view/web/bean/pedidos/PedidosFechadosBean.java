package view.web.bean.pedidos;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import basicas.negocio.Pedidos;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class PedidosFechadosBean extends GenericBean<Pedidos> {

	private List<Pedidos> listaBasicas;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setPedidosFechados(false);

		fachada = new Fachada();
		basica = new Pedidos();
		listaBasicas = fachada.listPedidos(false);
	}

	public Collection<Pedidos> getListaBasicas() {
		return listaBasicas;
	}

	public void listaItens(SelectEvent event) {
		try {
			basica = (Pedidos) event.getObject();
			basica.setListaItensPedido(fachada.listItens(basica));
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os itens do pedido! " + e.getMessage(), "criticalError");
		}
	}

	public Pedidos getBasica() {
		return basica;
	}

	public void setBasica(Pedidos basica) {
		this.basica = basica;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}