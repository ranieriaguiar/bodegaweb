package view.web.bean.pedidos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.Fornecedores;
import basicas.negocio.ItensPedido;
import basicas.negocio.ItensPedidoPK;
import basicas.negocio.Pedidos;
import basicas.negocio.Products;
import basicas.negocio.UnidadeMedida;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class PedidosBean extends GenericBean<Pedidos> {

	private Fornecedores fornecedor;
	private ItensPedido item;
	private UnidadeMedida unidMedida;
	private Products produto;
	private List<Products> listaProdutos;
	private List<CategoriaProduto> listaCategorias;
	private List<Fornecedores> listaFornecedores;
	private List<UnidadeMedida> listaUnidadeMedida;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setPedidosAbertos(false);

		fachada = new Fachada();
		unidMedida = new UnidadeMedida();
		fornecedor = new Fornecedores();
		item = new ItensPedido();
		produto = new Products();
		listaUnidadeMedida = fachada.listUnidMedida();
		listaFornecedores = fachada.listFornecedores();
		listaCategorias = fachada.listCategorias();
		basica = fachada.buscarUltimo(true);
		if (basica == null) {
			basica = new Pedidos();
		} else {
			basica.setListaItensPedido(fachada.listItens(basica));
		}
		listaProdutos = fachada.listProdutos();
	}

	public void salvar() {
		boolean erro = false;
		try {
			if (fornecedor == null || fornecedor.getNome().equals("")) {
				ReturnMessage.setMessage("Aten��o! Fornecedor est� nulo. Recarregue a p�gina.", "criticalError");
				erro = true;
			}
			if (usuario == null) {
				ReturnMessage.setMessage("Aten��o! Usu�rio est� nulo. Recarregue a p�gina.", "criticalError");
				erro = true;
			}
			if (!erro) {
				Pedidos novoPedido = new Pedidos();
				novoPedido.setFornecedor(fornecedor);
				novoPedido.setUsuario(usuario);
				fachada.save(novoPedido);
				ReturnMessage.setMessage("Pedido criado com sucesso.", "info");
				fornecedor = new Fornecedores();
				basica = fachada.buscarUltimo(true);
				RequestContext.getCurrentInstance().execute("PF('novoPedido').hide();");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao criar o pedido! " + e.getMessage(), "criticalError");
		}
	}

	public void inserirItem() {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um pedido.", "alert");
				erro = true;
			}
			if (produto == null || produto.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um produto.", "alert");
				erro = true;
			}
			if (item == null) {
				ReturnMessage.setMessage("Aten��o! Item est� nulo. Recarregue a p�gina.", "criticalError");
				erro = true;
			}
			if (!erro) {
				ItensPedidoPK chave = new ItensPedidoPK();
				chave.setPedido(basica);
				chave.setProduto(produto);
				item.setChaveComposta(chave);
				item.setUnidMedida(unidMedida);
				fachada.saveItem(item);
				item = new ItensPedido();
				// listaItens();
				produto = new Products();
				basica.setListaItensPedido(fachada.listItens(basica));
				// RequestContext.getCurrentInstance().execute("PF('novoProduto').hide();");
				ReturnMessage.setMessage("Item registrado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao registrar o item! " + e.getMessage(), "criticalError");
		}
	}

	public void apagarItem(ItensPedido itemPed) {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um pedido.", "alert");
				erro = true;
			}
			if (itemPed == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um item.", "alert");
				erro = true;
			}
			if (!erro) {
				fachada.deleteItem(itemPed);
				ReturnMessage.setMessage("Item apagado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao apagar o item! " + e.getMessage(), "criticalError");
		}
	}

	public void fecharPedido() {
		boolean erro = false;
		try {
			if (basica == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um pedido.", "alert");
				erro = true;
			}
			if (usuario == null) {
				ReturnMessage.setMessage("Aten��o! Usu�rio est� nulo. Recarregue a p�gina.", "criticalError");
				erro = true;
			}
			if (!erro) {
				basica.setTipoNota(Pedidos.notaNormal);
				basica.setUsuario(usuario);
				fachada.close(basica);
				basica = new Pedidos();
				RequestContext.getCurrentInstance().execute("PF('fecharPedido').hide();");
				ReturnMessage.setMessage("Pedido fechado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao fechar o pedido! " + e.getMessage(), "criticalError");
		}
	}

	public Collection<Fornecedores> getListaFornecedores() {
		try {
			return listaFornecedores;
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os fornecedores! " + e.getMessage(), "criticalError");
			return null;
		}
	}

	public Collection<UnidadeMedida> getListaUnidadeMedida() {
		try {
			return listaUnidadeMedida;
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar as Unidades de Medidas! " + e.getMessage(), "criticalError");
			return null;
		}
	}

	public Collection<Products> getListaProdutos() {
		return listaProdutos;
	}

	public List<String> getArrayListCategorias() {
		List<String> listaCategoriasString = new ArrayList<>();

		for (CategoriaProduto categoria : listaCategorias) {
			listaCategoriasString.add(categoria.getNome());
		}
		return listaCategoriasString;
	}

	public Fornecedores getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedores fornecedor) {
		this.fornecedor = fornecedor;
	}

	public ItensPedido getItem() {
		return item;
	}

	public void setItem(ItensPedido item) {
		this.item = item;
	}

	public Pedidos getBasica() {
		return basica;
	}

	public void setBasica(Pedidos basica) {
		this.basica = basica;
	}

	public UnidadeMedida getUnidMedida() {
		return unidMedida;
	}

	public void setUnidMedida(UnidadeMedida unidMedida) {
		this.unidMedida = unidMedida;
	}

	public Products getProduto() {
		return produto;
	}

	public void setProduto(Products produto) {
		this.produto = produto;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}