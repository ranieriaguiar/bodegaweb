package view.web.bean.pedidos;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import basicas.negocio.Fornecedores;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class FornecedoresBean extends GenericBean<Fornecedores> {

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setFornecedores(false);

		basica = new Fornecedores();
		fachada = new Fachada();
	}

	public void limpar() {
		basica = new Fornecedores();
	}

	public void apagar() {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um fornecedor.", "alert");
				erro = true;
			}
			if (!erro) {
				fachada.delete(basica);
				ReturnMessage.setMessage("Fornecedor apagado com sucesso.", "info");
				limpar();
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao apagar o fornecedor! " + e.getMessage(), "criticalError");
		}
	}

	public void salvar() {
		boolean erro = false;
		try {
			if (basica == null) {
				ReturnMessage.setMessage("Objeto 'fornecedor' est� nulo. Tente mais tarde.", "erro");
				erro = true;
			}
			if (!erro) {
				if (basica.getId() == null) {
					fachada.save(basica);
					ReturnMessage.setMessage("Fornecedor salvo com sucesso.", "info");
				} else {
					fachada.update(basica);
					ReturnMessage.setMessage("Fornecedor alterado com sucesso.", "info");
				}
				limpar();
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao salvar o fornecedor! " + e.getMessage(), "criticalError");
		}
	}

	public Collection<Fornecedores> getListaBasicas() {
		try {
			return fachada.listFornecedores();
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os fornecedores! " + e.getMessage(), "criticalError");
			return null;
		}
	}

	public Fornecedores getBasica() {
		return basica;
	}

	public void setBasica(Fornecedores basica) {
		this.basica = basica;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}