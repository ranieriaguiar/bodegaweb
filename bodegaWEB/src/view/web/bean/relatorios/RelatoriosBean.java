package view.web.bean.relatorios;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class RelatoriosBean<T> extends GenericBean<T> {

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setRelatorios(false);

		fachada = new Fachada();
	}

	public void relatorioEstoque() {
		fachada.contarEstoque();
	}
	
	public void relatorioMovimentacoes() {
		fachada.contarMovimentacoes();
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}