package view.web.bean.relatorios;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class GraficosBean<T> extends GenericBean<T> {

	private LineChartModel animatedModel;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setGraficos(false);

		createAnimatedModels();
		fachada = new Fachada();
	}

	public LineChartModel getAnimatedModel() {
		return animatedModel;
	}

	private void createAnimatedModels() {
		animatedModel = initLinearModel();
		animatedModel.setTitle("Gr�ficos de Vendas");
		animatedModel.setAnimate(true);
		animatedModel.setLegendPosition("se");
		Axis yAxis = animatedModel.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(10);
	}

	private LineChartModel initLinearModel() {
		LineChartModel model = new LineChartModel();

		LineChartSeries series1 = new LineChartSeries();
		series1.setLabel("Pedidos");

		series1.set(1, 2);
		series1.set(2, 1);
		series1.set(3, 3);
		series1.set(4, 6);
		series1.set(5, 8);

		LineChartSeries series2 = new LineChartSeries();
		series2.setLabel("Vendas");

		series2.set(1, 6);
		series2.set(2, 3);
		series2.set(3, 2);
		series2.set(4, 7);
		series2.set(5, 9);

		model.addSeries(series1);
		model.addSeries(series2);

		return model;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}