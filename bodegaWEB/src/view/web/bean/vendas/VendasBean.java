package view.web.bean.vendas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.Clientes;
import basicas.negocio.ItensVenda;
import basicas.negocio.ItensVendaPK;
import basicas.negocio.Products;
import basicas.negocio.Vendas;
import basicas.sistema.Permissoes;
import basicas.sistema.RegraNegocio;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class VendasBean extends GenericBean<Vendas> {

	private Clientes cliente;
	private ItensVenda item;
	private boolean tipoPag;
	private RegraNegocio regras;
	private List<Vendas> listaBasicas;
	private List<Products> listaProdutos;
	private List<CategoriaProduto> listCategorias;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setVendasAbertas(false);

		fachada = new Fachada();
		regras = fachada.buscarRegras();
		listCategorias = fachada.listCategorias();
		listaBasicas = fachada.listVendas(true);
		listaProdutos = fachada.listProdutosMaisVendidos();
		cliente = new Clientes();
		item = new ItensVenda();
		basica = new Vendas();
	}

	public void salvar() {
		boolean erro = false;
		try {
			if (cliente == null || cliente.getNome().equals("")) {
				ReturnMessage.setMessage("Aten��o! Item est� nulo. Tente mais tarde.", "criticalError");
				erro = true;
			}
			if (!erro) {
				Vendas novaVenda = new Vendas();
				novaVenda.setCliente(cliente);
				fachada.save(novaVenda);
				ReturnMessage.setMessage("Venda criada com sucesso.", "info");
				cliente = new Clientes();
				listaBasicas = fachada.listVendas(true);
				RequestContext.getCurrentInstance().execute("PF('novaVenda').hide();");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao criar a venda! " + e.getMessage(), "criticalError");
		}
	}

	public void isFrios(Products prod) {
		if (prod.getId().equals(regras.getPratoFrios())) {
			RequestContext.getCurrentInstance().execute("PF('novoItem').show();");
		} else {
			inserirItem(prod);
		}
	}

	public void inserirFrios() {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione uma venda.", "alert");
				erro = true;
			}
			if (item == null) {
				ReturnMessage.setMessage("Aten��o! Item est� nulo. Tente mais tarde.", "criticalError");
				erro = true;
			}
			if (!erro) {
				ItensVenda novoItem = new ItensVenda();
				Products prod = new Products();
				prod.setId(regras.getPratoFrios());
				novoItem.getChaveComposta().setProduto(prod);
				novoItem.getChaveComposta().setVenda(basica);
				novoItem.setQtd(getItem().getQtd());
				fachada.saveItem(novoItem);
				ReturnMessage.setMessage("Item registrado com sucesso.", "info");
				listaItens();
				RequestContext.getCurrentInstance().execute("PF('novoItem').hide();");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao registrar o item! " + e.getMessage(), "criticalError");
		}
	}

	private void inserirItem(Products prod) {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione uma venda.", "alert");
				erro = true;
			}
			if (prod == null || prod.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um produto.", "alert");
				erro = true;
			}
			if (!erro) {
				ItensVendaPK chave = new ItensVendaPK();
				ItensVenda novoItem = new ItensVenda();
				chave.setVenda(basica);
				chave.setProduto(prod);
				novoItem.setChaveComposta(chave);
				fachada.saveItem(novoItem);
				ReturnMessage.setMessage("Item registrado com sucesso.", "info");
				listaItens();
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao registrar o item! " + e.getMessage(), "criticalError");
		}
	}

	public void apagarItem(ItensVenda itemVen) {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione uma venda.", "alert");
				erro = true;
			}
			if (itemVen == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um item.", "alert");
				erro = true;
			}
			if (!erro) {
				fachada.deleteItem(itemVen);
				ReturnMessage.setMessage("Item apagado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao apagar o item! " + e.getMessage(), "criticalError");
		}
	}

	public void aumentarItem(ItensVenda itemVen) {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione uma venda.", "alert");
				erro = true;
			}
			if (itemVen == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um item.", "alert");
				erro = true;
			}
			if (!erro) {
				fachada.increaseItem(itemVen);
				ReturnMessage.setMessage("Item incrementado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao incrementar o item! " + e.getMessage(), "criticalError");
		}
	}

	public void diminuirItem(ItensVenda itemVen) {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione uma venda.", "alert");
				erro = true;
			}
			if (itemVen == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um item.", "alert");
				erro = true;
			}
			if (!erro) {
				fachada.decreaseItem(itemVen);
				ReturnMessage.setMessage("Item decrementado com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao decrementar o item! " + e.getMessage(), "criticalError");
		}
	}

	public void confirmarVenda(Vendas ven) {
		try {
			basica = ven;
			RequestContext.getCurrentInstance().execute("PF('fecharVenda').show();");
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao fechar venda! " + e.getMessage(), "criticalError");
		}
	}

	public void fecharVenda() {
		boolean erro = false;
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione uma venda.", "alert");
				erro = true;
			}
			if (!erro) {
				if (tipoPag) {
					basica.setTipoPag(0);
				} else {
					basica.setTipoPag(1);
				}
				fachada.close(basica);
				ReturnMessage.setMessage("Venda fechada com sucesso.", "info");
				basica = new Vendas();
				listaBasicas = fachada.listVendas(true);
				RequestContext.getCurrentInstance().execute("PF('fecharVenda').hide();");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao fechar a venda! " + e.getMessage(), "criticalError");
		}
	}

	public Collection<Vendas> getListaBasicas() {
		return listaBasicas;
	}

	public void listaItens() {
		try {
			this.basica.setListaItensVenda(fachada.listItens(basica));
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os itens da venda! " + e.getMessage(), "criticalError");
		}
	}

	public Collection<Products> getListaProdutos() {
		return listaProdutos;
	}

	public List<String> getArrayListCategorias() {
		List<String> listaCategorias = new ArrayList<>();

		for (CategoriaProduto categoria : listCategorias) {
			listaCategorias.add(categoria.getNome());
		}
		return listaCategorias;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public ItensVenda getItem() {
		return item;
	}

	public void setItem(ItensVenda item) {
		this.item = item;
	}

	public Vendas getBasica() {
		return basica;
	}

	public void setBasica(Vendas basica) {
		this.basica = basica;
	}

	public boolean isTipoPag() {
		return tipoPag;
	}

	public void setTipoPag(boolean tipoPag) {
		this.tipoPag = tipoPag;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}