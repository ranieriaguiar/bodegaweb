package view.web.bean.vendas;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import basicas.negocio.Vendas;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class VendasFechadasBean extends GenericBean<Vendas> {

	private List<Vendas> listaBasicas;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setVendasFechadas(false);

		fachada = new Fachada();
		basica = new Vendas();
		listaBasicas = fachada.listVendas(false);
	}

	public Collection<Vendas> getListaBasicas() {
		return listaBasicas;
	}

	public void listaItens(SelectEvent event) {
		try {
			basica = (Vendas) event.getObject();
			basica.setListaItensVenda(fachada.listItens(basica));
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os itens da venda! " + e.getMessage(), "criticalError");
		}
	}

	public Vendas getBasica() {
		return basica;
	}

	public void setBasica(Vendas basica) {
		this.basica = basica;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}