package view.web.bean;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import basicas.sistema.Permissoes;
import basicas.sistema.Usuario;
import facade.Fachada;

public abstract class GenericBean<T> {

	protected Fachada fachada;
	protected T basica;
	protected Permissoes permissoes;
	protected Usuario usuario;

	protected void listarPermissoes() {
		permissoes = new Permissoes();

		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		usuario = (Usuario) session.getAttribute("usuarioLogado");

		permissoes.converterPermissoes(usuario.getListaPermissoes());
	}

}
