package view.web.bean.estoque;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.ItEntradaPrateleira;
import basicas.negocio.Products;
import basicas.negocio.UnidadeMedida;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class MovimentacoesBean extends GenericBean<Products> {

	private ItEntradaPrateleira item;
	private List<Products> listProdutos;
	private List<ItEntradaPrateleira> listaItensPrateleira;
	private List<UnidadeMedida> listUnidadeMedida;
	private List<CategoriaProduto> listCategorias;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setMovimentacao(false);

		fachada = new Fachada();
		listProdutos = fachada.listProdutos();
		listUnidadeMedida = fachada.listUnidMedida();
		listaItensPrateleira = new ArrayList<>();
		listCategorias = new ArrayList<>();
		basica = new Products();
		item = new ItEntradaPrateleira();
	}

	public Collection<ItEntradaPrateleira> getListaItensPrateleira() {
		try {
			if (listaItensPrateleira.size() == 0) {
				listaItensPrateleira = fachada.listPrateleira();
			}
			return listaItensPrateleira;
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar as movimenta��es! " + e.getMessage(), "criticalError");
			return null;
		}
	}

	public Collection<Products> getListaBasicas() {
		try {
			return listProdutos;
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os produtos! " + e.getMessage(), "criticalError");
			return null;
		}
	}

	public List<String> getArrayListCategorias() {
		List<String> listaCategorias = new ArrayList<>();

		for (CategoriaProduto categoria : listCategorias) {
			listaCategorias.add(categoria.getNome());
		}
		return listaCategorias;
	}

	public void inserirItem() {
		try {
			if (basica == null || basica.getId() == null) {
				ReturnMessage.setMessage("Aten��o! Selecione um produto.", "alert");
			} else if (item == null) {
				ReturnMessage.setMessage("Aten��o! Item est� nulo. Recarregue a p�gina.", "criticalError");
			} else if (item.getUnidMedida() == null || item.getUnidMedida().getId() == null) {
				ReturnMessage.setMessage("Aten��o! Unidade de medida est� nula. Recarregue a p�gina.", "criticalError");
			} else if (usuario == null) {
				ReturnMessage.setMessage("Aten��o! Usu�rio est� nulo. Recarregue a p�gina.", "criticalError");
			} else {
				item.setProduto(basica);
				item.setUsuario(usuario);
				fachada.save(item);
				item = new ItEntradaPrateleira();
				basica = new Products();
				listaItensPrateleira = fachada.listPrateleira();
				ReturnMessage.setMessage("Movimenta��o registrada com sucesso.", "info");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao registrar o item! " + e.getMessage(), "criticalError");
		}
	}

	public Products getBasica() {
		return basica;
	}

	public void setBasica(Products basica) {
		this.basica = basica;
	}

	public ItEntradaPrateleira getItem() {
		return item;
	}

	public void setItem(ItEntradaPrateleira item) {
		this.item = item;
	}

	public List<Products> getListProdutos() {
		return listProdutos;
	}

	public List<UnidadeMedida> getListaUnidadeMedida() {
		return listUnidadeMedida;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}