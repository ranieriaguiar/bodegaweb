package view.web.bean.estoque;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

import org.primefaces.context.RequestContext;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.Products;
import basicas.sistema.Permissoes;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.bean.GenericBean;

@ManagedBean(eager = true)
@ViewScoped
public class ProdutosBean extends GenericBean<Products> {

	private List<Products> listProdutos;
	private List<CategoriaProduto> listCategorias;

	@PostConstruct
	public void init() {
		listarPermissoes();
		permissoes.setProdutos(false);

		fachada = new Fachada();
		listCategorias = this.fachada.listCategorias();
		listProdutos = fachada.listProdutos();
		basica = new Products();
	}

	public void limpar() {
		basica = new Products();
	}

	public void salvar() {
		try {
			if (basica == null) {
				ReturnMessage.setMessage("Objeto 'produto' est� nulo. Tente mais tarde.", "erro");
			} else {
				if (basica.getId() == null) {
					fachada.save(basica);
					listProdutos = fachada.listProdutos();
					RequestContext.getCurrentInstance().execute("PF('novoProduto').hide();");
					ReturnMessage.setMessage("Produto salvo com sucesso.", "info");
				} else {
					fachada.update(basica);
					listProdutos = fachada.listProdutos();
					RequestContext.getCurrentInstance().execute("PF('novoProduto').hide();");
					ReturnMessage.setMessage("Produto alterado com sucesso.", "info");
				}
				limpar();
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao salvar o produto! " + e.getMessage(), "criticalError");
		}
	}

	public void editar(Products prod) {
		if (prod == null) {
			ReturnMessage.setMessage("Objeto 'produto' est� nulo. Tente mais tarde.", "erro");

		} else {
			basica = prod;
			RequestContext.getCurrentInstance().execute("PF('novoProduto').show();");
		}
	}

	public void apagar(Products prod) {
		try {
			if (prod == null) {
				ReturnMessage.setMessage("Objeto 'produto' est� nulo. Tente mais tarde.", "erro");
			} else {
				fachada.delete(prod);
				listProdutos = fachada.listProdutos();
				ReturnMessage.setMessage("Produto apagado com sucesso.", "info");
				limpar();
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao apagar o produto! " + e.getMessage(), "criticalError");
		}
	}

	public Collection<Products> getListaBasicas() {
		try {
			return listProdutos;
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao listar os produtos! " + e.getMessage(), "criticalError");
			return null;
		}
	}

	public List<String> getArrayListCategorias() {
		List<String> listaCategorias = new ArrayList<>();

		for (CategoriaProduto categoria : listCategorias) {
			listaCategorias.add(categoria.getNome());
		}
		return listaCategorias;
	}

	public List<SelectItem> getListaCategorias() {
		List<SelectItem> listItemCateg = new ArrayList<SelectItem>();
		int quantidadeGrupos = 0;

		for (CategoriaProduto categoria : listCategorias) {
			if (categoria.getSubCategoriaProd().getId() > quantidadeGrupos) {
				quantidadeGrupos = categoria.getSubCategoriaProd().getId();
			}
		}

		int indexCategoria;
		int[] arrayIndexCategoria = new int[quantidadeGrupos];
		String nomeCategoria = "";

		for (int x = 0; x < quantidadeGrupos; x++) {

			for (CategoriaProduto categoriaProd : listCategorias) {
				if (categoriaProd.getSubCategoriaProd().getId() == (x + 1)) {
					arrayIndexCategoria[x]++;
				}
			}
		}

		for (int i = 1; i <= quantidadeGrupos; i++) {
			SelectItem[] arrayCategoria = new SelectItem[arrayIndexCategoria[i - 1]];
			indexCategoria = 0;

			for (CategoriaProduto categoriaProd : listCategorias) {
				if (categoriaProd.getSubCategoriaProd().getId() == i) {
					arrayCategoria[indexCategoria] = new SelectItem(categoriaProd.getId(), categoriaProd.getNome());
					indexCategoria++;
					if (indexCategoria == arrayIndexCategoria[i - 1]) {
						nomeCategoria = categoriaProd.getSubCategoriaProd().getNome();
						break;
					}
				}
			}

			SelectItemGroup g1 = new SelectItemGroup(nomeCategoria);
			g1.setSelectItems(arrayCategoria);
			listItemCateg.add(g1);
			nomeCategoria = "";
		}

		return listItemCateg;
	}

	public Products getBasica() {
		return basica;
	}

	public void setBasica(Products basica) {
		this.basica = basica;
	}

	public Permissoes getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Permissoes permissoes) {
		this.permissoes = permissoes;
	}
}