package view.web.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import basicas.sistema.Usuario;
import facade.Fachada;
import view.web.ReturnMessage;
import view.web.session.SessionContext;

@ManagedBean(eager = true)
@ViewScoped
public class LoginBean {

	private Fachada fachada;
	private Usuario basica;
	private String erroMessage;

	@PostConstruct
	public void init() {
		fachada = new Fachada();
		basica = new Usuario();
		erroMessage = "*campos obrigat�rios";
	}

	public String verificarLogin() {
		Usuario usuario;
		try {
			usuario = fachada.verificaLogin(basica);

			switch (usuario.getStatusCode()) {
			case Usuario.loginOk:
				SessionContext.getInstance().setAttribute("usuarioLogado", usuario);
				return usuario.getHomePage().getUrl();
			case Usuario.userAndPasswordDoesntMatch:
				erroMessage = "Usu�rio e senha n�o coincidem!";
				return "";
			case Usuario.userDoesntExist:
				erroMessage = "Usu�rio n�o existe!";
				return "";
			default:
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao verificar login! " + e.getMessage(), "criticalError");
			return "";
		}
	}

	public String getDoLogout() {
		try {
			SessionContext.getInstance().encerrarSessao();
			return "/login.xhtml?faces-redirect=true";
		} catch (Exception e) {
			e.printStackTrace();
			ReturnMessage.setMessage("Erro ao realizar logout! " + e.getMessage(), "criticalError");
			return "";
		}
	}

	public Usuario getBasica() {
		return basica;
	}

	public void setBasica(Usuario basica) {
		this.basica = basica;
	}

	public String getErroMessage() {
		return erroMessage;
	}

	public void setErroMessage(String erroMessage) {
		this.erroMessage = erroMessage;
	}

}
