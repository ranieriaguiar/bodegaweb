package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/estoque/produtos.xhtml")
public class ProdutosFilter extends GenericFilter {

	public ProdutosFilter() {
		pagina = "produtos";
	}

}
