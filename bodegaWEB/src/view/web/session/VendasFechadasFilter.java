package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/vendas/vendasFechadas.xhtml")
public class VendasFechadasFilter extends GenericFilter {

	public VendasFechadasFilter() {
		pagina = "vendasFechadas";
	}

}
