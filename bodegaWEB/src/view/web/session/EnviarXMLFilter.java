package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/pedidos/enviarXML.xhtml")
public class EnviarXMLFilter extends GenericFilter {

	public EnviarXMLFilter() {
		pagina = "enviarXML";
	}

}
