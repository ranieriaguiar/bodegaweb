package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/pedidos/fornecedores.xhtml")
public class FornecedoresFilter extends GenericFilter {

	public FornecedoresFilter() {
		pagina = "fornecedores";
	}

}
