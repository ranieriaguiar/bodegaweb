package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/pedidos/pedidosFechados.xhtml")
public class PedidosFechadosFilter extends GenericFilter {

	public PedidosFechadosFilter() {
		pagina = "pedidosFechados";
	}

}
