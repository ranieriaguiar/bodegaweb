package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/vendas/vendas.xhtml")
public class VendasFilter extends GenericFilter {

	public VendasFilter() {
		pagina = "vendas";
	}

}
