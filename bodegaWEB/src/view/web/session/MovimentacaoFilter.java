package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/estoque/movimentacao.xhtml")
public class MovimentacaoFilter extends GenericFilter {

	public MovimentacaoFilter() {
		pagina = "movimentacao";
	}

}
