package view.web.session;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import basicas.sistema.Permissoes;
import basicas.sistema.Usuario;

public abstract class GenericFilter implements Filter {

	protected String pagina;

	public GenericFilter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession sessao = req.getSession();

		if (sessao == null || sessao.getAttribute("usuarioLogado") == null
				|| ((Usuario) sessao.getAttribute("usuarioLogado")) == null) {
			RequestDispatcher dis = request.getRequestDispatcher("/login.xhtml");
			dis.forward(request, response);
		} else {

			boolean autorizado = false;
			List<Permissoes> listaPermissoes = ((Usuario) sessao.getAttribute("usuarioLogado")).getListaPermissoes();

			if (listaPermissoes != null && listaPermissoes.size() > 0) {
				for (Permissoes permissao : listaPermissoes) {

					if (permissao.getPagina().getNome().equals(pagina)) {
						autorizado = true;
						chain.doFilter(request, response);
					}
				}
			}

			if (!autorizado) {
				RequestDispatcher dis = request
						.getRequestDispatcher(((Usuario) sessao.getAttribute("usuarioLogado")).getHomePage().getUrl());
				dis.forward(request, response);
			}
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
