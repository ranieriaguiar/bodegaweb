package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/relatorios/relatorios.xhtml")
public class RelatoriosFilter extends GenericFilter {

	public RelatoriosFilter() {
		pagina = "relatorios";
	}

}
