package view.web.session;

import javax.servlet.annotation.WebFilter;

@WebFilter("/pedidos/pedidos.xhtml")
public class PedidosFilter extends GenericFilter {

	public PedidosFilter() {
		pagina = "pedidos";
	}

}
