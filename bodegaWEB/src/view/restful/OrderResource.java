package view.restful;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import basicas.negocio.Fornecedores;
import basicas.negocio.ItensPedido;
import basicas.negocio.Pedidos;
import basicas.negocio.UnidadeMedida;
import facade.Fachada;

@Path("/order")
public class OrderResource {

	private Fachada fachada = new Fachada();

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Pedidos> listaPedidos() {
		return fachada.listPedidos(false);
	}

	@GET
	@Path("/provider")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Fornecedores> listaFornecedores() {
		return fachada.listFornecedores();
	}

	@GET
	@Path("/orderitems")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ItensPedido> listaItensPedidos() {
		return fachada.listItens();
	}
	
	@GET
	@Path("/unitmeasurement")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UnidadeMedida> listaUnidMedida() {
		return fachada.listUnidMedida();
	}
}
