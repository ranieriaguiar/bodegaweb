package view.restful;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import basicas.mobile.UsuarioMobile;
import basicas.sistema.Usuario;
import facade.Fachada;

@Path("/login")
public class LoginRest {

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response login(UsuarioMobile usuario) {
		Fachada fachada = new Fachada();
		Status status = Status.INTERNAL_SERVER_ERROR;
		try {
			usuario = fachada.verificaLogin(usuario);
			return Response.ok(usuario).build();
		} catch (Exception e) {
			e.printStackTrace();
			usuario = new UsuarioMobile();
			usuario.setStatusCode(Usuario.serverError);
			return Response.ok(usuario).build();
		}
	}
}