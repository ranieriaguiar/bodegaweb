package view.restful;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import basicas.mobile.UsuarioMobile;
import basicas.negocio.CategoriaProduto;
import basicas.negocio.ItEntradaPrateleira;
import basicas.negocio.Products;
import basicas.negocio.SubCategoriaProd;
import facade.Fachada;
import facade.IFachada;

@Path("/products")
public class ProductsResource {

	@POST
	@Path("/post")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response postProducts(List<Products> listProducts, UsuarioMobile user) {
		IFachada fachada = new Fachada();
		Status status;
		try {
			fachada.update(listProducts);
			status = Status.OK;
			return Response.status(status).build();
		} catch (Exception e) {
			e.printStackTrace();
			status = Status.INTERNAL_SERVER_ERROR;
			return Response.status(status).build();
		}
	}

	@POST
	@Path("/postStockMovement")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response postStockMovement(List<ItEntradaPrateleira> listStockMovement, UsuarioMobile user) {
		IFachada fachada = new Fachada();
		Status status;
		try {
			fachada.save(listStockMovement);
			status = Status.OK;
			return Response.status(status).build();
		} catch (Exception e) {
			e.printStackTrace();
			status = Status.INTERNAL_SERVER_ERROR;
			return Response.status(status).build();
		}
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Products> listaProdutos(Date data, UsuarioMobile user) {
		IFachada fachada = new Fachada();
		if (data == null) {
			return fachada.listProdutos(user);
		} else {
			return fachada.listProdutos(data, user);
		}
	}

	@GET
	@Path("/categorias")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CategoriaProduto> listaCategorias() {
		IFachada fachada = new Fachada();
		return fachada.listCategorias();
	}

	@GET
	@Path("/subcategorias")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SubCategoriaProd> listaSubCategorias() {
		IFachada fachada = new Fachada();
		return fachada.listSubCategorias();
	}
}
