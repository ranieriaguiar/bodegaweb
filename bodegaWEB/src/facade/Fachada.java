package facade;

import basicas.mobile.UsuarioMobile;

//  @author Ranieri

import basicas.negocio.*;
import basicas.sistema.*;
import controller.*;

import java.util.Date;
import java.util.List;

public class Fachada implements IFachada {

	// Usuario

	public Usuario verificaLogin(Usuario u) {
		UsuarioRN rnUsuario = new UsuarioRN();
		return rnUsuario.verificarLogin(u);
	}

	// Mobile
	public UsuarioMobile verificaLogin(UsuarioMobile u) {
		UsuarioRN rnUsuario = new UsuarioRN();
		return rnUsuario.verificarLogin(u);
	}

	public RegraNegocio buscarRegras() {
		UsuarioRN rnUsuario = new UsuarioRN();
		return rnUsuario.buscarRegras();
	}

	// Produtos

	public void save(Products p) {
		ProdutoRN rnProduto = new ProdutoRN();
		rnProduto.salvar(p);
	}

	public Products select(Products p) {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.selecionar(p);
	}

	public void update(Products p) {
		ProdutoRN rnProduto = new ProdutoRN();
		rnProduto.alterar(p);
	}

	// Mobile
	public void update(List<Products> list) {
		ProdutoRN rnProduto = new ProdutoRN();
		rnProduto.alterar(list);
	}

	public void delete(Products p) {
		ProdutoRN rnProduto = new ProdutoRN();
		rnProduto.excluir(p);
	}

	public List<Products> listProdutos() {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listar();
	}

	public List<Products> listProdutosMaisVendidos() {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listarMaisVendidos();
	}

	@Override
	public List<Products> listProdutos(boolean ativado) {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listar(ativado);
	}

	// Mobile
	public List<Products> listProdutos(Date data, UsuarioMobile user) {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listar(data, user);
	}

	// Mobile
	public List<Products> listProdutos(UsuarioMobile user) {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listar(user);
	}

	public List<Products> listProdutos(CategoriaProduto categoria) {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listar(categoria);
	}

	public List<Products> listProdutos(CategoriaProduto categoria, boolean ativado) {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listar(categoria, ativado);
	}

	// Mobile
	public List<CategoriaProduto> listCategorias() {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listarCategorias();
	}

	// Mobile
	public List<SubCategoriaProd> listSubCategorias() {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listarSubCategorias();
	}

	// Prateleira

	public List<ItEntradaPrateleira> listPrateleira() {
		ProdutoRN rnProduto = new ProdutoRN();
		return rnProduto.listarPrateleira();
	}

	public void save(ItEntradaPrateleira i) {
		ProdutoRN rnProduto = new ProdutoRN();
		rnProduto.salvar(i);
	}

	// Mobile
	public void save(List<ItEntradaPrateleira> listPrateleira) {
		ProdutoRN rnProduto = new ProdutoRN();
		rnProduto.salvar(listPrateleira);
	}

	// Vendas

	public void save(Vendas v) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.salvar(v);
	}

	@Override
	public void close(Vendas v) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.fechar(v);
	}

	@Override
	public void delete(Vendas v) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.excluir(v);
	}

	@Override
	public Vendas buscarUltima(boolean aberta) {
		VendaRN rnVenda = new VendaRN();
		return rnVenda.buscarUltima(aberta);
	}

	@Override
	public List<Vendas> listVendas(boolean aberto) {
		VendaRN rnVenda = new VendaRN();
		return rnVenda.listarVendas(aberto);
	}

	// Itens da Venda

	@Override
	public void saveItem(ItensVenda itVen) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.salvarItens(itVen);
	}

	@Override
	public void deleteItem(ItensVenda itVen) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.apagarItem(itVen);
	}

	@Override
	public void increaseItem(ItensVenda itVen) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.aumentarItem(itVen);
	}

	@Override
	public void decreaseItem(ItensVenda itVen) {
		VendaRN rnVenda = new VendaRN();
		rnVenda.diminuirItem(itVen);
	}

	@Override
	public List<ItensVenda> listItens(Vendas v) {
		VendaRN rnVenda = new VendaRN();
		return rnVenda.listaItens(v);
	}

	// Pedidos

	public void save(Pedidos p) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.salvarPedido(p);
	}

	@Override
	public void close(Pedidos p) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.fecharPedido(p);
	}

	@Override
	public void delete(Pedidos p) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.excluirPedido(p);
	}

	// Mobile
	@Override
	public List<Pedidos> listPedidos(boolean aberto) {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.listarPedidos(aberto);
	}

	@Override
	public Pedidos buscarUltimo(boolean aberto) {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.buscarUltimo(aberto);
	}

	public Pedidos verificarProdNotasXML(Pedidos p) {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.verificarProdNotasXML(p);
	}

	public void salvarNotaXML(Pedidos p) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.salvarNotaXML(p);
	}

	// Itens do Pedido

	@Override
	public void saveItem(ItensPedido itPed) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.salvarItemPedido(itPed);
	}

	@Override
	public void deleteItem(ItensPedido itPed) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.apagarItem(itPed);
	}

	@Override
	public List<ItensPedido> listItens(Pedidos p) {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.listaItensPedido(p);
	}

	// Mobile
	@Override
	public List<ItensPedido> listItens() {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.listaItensPedido();
	}

	// Mobile
	public List<UnidadeMedida> listUnidMedida() {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.listarUnidMedida();
	}

	// Fornecedores

	public void save(Fornecedores f) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.salvarFornecedor(f);
	}

	public void update(Fornecedores f) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.alterarFornecedor(f);
	}

	public void delete(Fornecedores f) {
		PedidoRN rnPedido = new PedidoRN();
		rnPedido.excluirFornecedor(f);
	}

	// Mobile
	public List<Fornecedores> listFornecedores() {
		PedidoRN rnPedido = new PedidoRN();
		return rnPedido.listFornecedores();
	}

	// Relatórios

	public void contarEstoque() {
		RelatorioRN rnRelatorio = new RelatorioRN();
		rnRelatorio.gerarEstoque();
	}

	public void contarMovimentacoes() {
		RelatorioRN rnRelatorio = new RelatorioRN();
		rnRelatorio.gerarMovimentacoes();
	}
}