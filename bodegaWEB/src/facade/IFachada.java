package facade;

import basicas.mobile.UsuarioMobile;
import basicas.negocio.*;
import basicas.sistema.*;

import java.util.Date;
import java.util.List;

//  @author Ranieri
public interface IFachada {

	// Login

	public Usuario verificaLogin(Usuario u);

	public UsuarioMobile verificaLogin(UsuarioMobile u);

	public RegraNegocio buscarRegras();

	// Produtos

	public void save(Products p);

	public Products select(Products p);

	public void update(Products p);

	public void update(List<Products> list);

	public void delete(Products p);

	public List<Products> listProdutos();

	public List<Products> listProdutosMaisVendidos();

	public List<Products> listProdutos(boolean ativado);

	public List<Products> listProdutos(Date data, UsuarioMobile user);

	public List<Products> listProdutos(UsuarioMobile user);

	public List<Products> listProdutos(CategoriaProduto categoria);

	public List<Products> listProdutos(CategoriaProduto categoria, boolean ativado);

	public List<CategoriaProduto> listCategorias();

	public List<SubCategoriaProd> listSubCategorias();

	// Prateleira

	public List<ItEntradaPrateleira> listPrateleira();

	public void save(ItEntradaPrateleira i);

	public void save(List<ItEntradaPrateleira> listPrateleira);

	// Vendas

	public void save(Vendas v);

	public void close(Vendas v);

	public void delete(Vendas v);
	
	public Vendas buscarUltima(boolean aberta);

	public List<Vendas> listVendas(boolean aberto);

	// Itens da Venda

	public void saveItem(ItensVenda itVen);

	public void deleteItem(ItensVenda itVen);

	public void increaseItem(ItensVenda itVen);

	public void decreaseItem(ItensVenda itVen);

	public List<ItensVenda> listItens(Vendas v);

	// Pedidos

	public void save(Pedidos p);

	public void close(Pedidos p);

	public void delete(Pedidos p);

	public List<Pedidos> listPedidos(boolean aberto);

	public Pedidos buscarUltimo(boolean aberto);

	public Pedidos verificarProdNotasXML(Pedidos p);

	public void salvarNotaXML(Pedidos p);

	// Itens do Pedido

	public void saveItem(ItensPedido itPed);

	public void deleteItem(ItensPedido itPed);

	public List<ItensPedido> listItens(Pedidos p);

	public List<ItensPedido> listItens();

	public List<UnidadeMedida> listUnidMedida();

	// Fornecedores

	public void save(Fornecedores f);

	public void update(Fornecedores f);

	public void delete(Fornecedores f);

	public List<Fornecedores> listFornecedores();

	// Relatórios

	public void contarEstoque();

	public void contarMovimentacoes();	
}
