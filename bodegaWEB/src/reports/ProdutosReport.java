package reports;

import java.util.List;

import basicas.negocio.Products;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ProdutosReport {

	private String path;
	private String pathToReportPackage;
	public static final String simples = "Produtos";
	public static final String complexa = "Produtos2";

	public ProdutosReport() {
		this.path = this.getClass().getClassLoader().getResource("").getPath();
		this.pathToReportPackage = this.path + "jasper/";
		System.out.println(path);
	}

	public void imprimir(List<Products> produtos, String tipoRelatorio) throws Exception {

		JasperReport report = JasperCompileManager
				.compileReport(this.getPathToReportPackage() + tipoRelatorio + ".jrxml");

		JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(produtos, false));

		JasperExportManager.exportReportToPdfFile(print, "C:/Users/Bernadete/Documents/" + tipoRelatorio + ".pdf");
	}

	public String getPathToReportPackage() {
		return this.pathToReportPackage;
	}

	public String getPath() {
		return this.path;
	}

}
