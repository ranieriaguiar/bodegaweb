package daoGeral;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

public abstract class DAOGenerico<T> {
	private EntityManager entityManager;
	private Class<T> classePersistente;

	@SuppressWarnings("unchecked")
	public DAOGenerico(EntityManager em) {
		this.setEntityManager(em);
		ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
		classePersistente = (Class<T>) parameterizedType.getActualTypeArguments()[0];
	}

	public void inserir(T objeto) {
		EntityTransaction tx = getEntityManager().getTransaction();
		try {
			tx.begin();
			getEntityManager().persist(objeto);
			tx.commit();
		} catch (PersistenceException e) {
			tx.rollback();
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a inser��o no banco de dados!");
		}
	}

	public final void inserirColecao(Collection<T> lista) {
		EntityTransaction tx = getEntityManager().getTransaction();
		try {
			tx.begin();
			for (T entidade : lista) {
				getEntityManager().persist(entidade);
			}
			tx.commit();
		} catch (PersistenceException e) {
			tx.rollback();
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a inser��o no banco de dados!");
		}
	}

	public T alterar(T objeto) {
		EntityTransaction tx = getEntityManager().getTransaction();
		try {
			tx.begin();
			objeto = getEntityManager().merge(objeto);
			tx.commit();
		} catch (PersistenceException e) {
			tx.rollback();
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a altera��o no banco de dados!");
		}
		return objeto;
	}

	public final void remover(T objeto) {
		EntityTransaction tx = getEntityManager().getTransaction();
		try {
			tx.begin();
			objeto = getEntityManager().merge(objeto);
			getEntityManager().remove(objeto);
			tx.commit();
		} catch (PersistenceException e) {
			tx.rollback();
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o no banco de dados!");
		}
	}

	public final T buscarPorChave(Serializable id) {
		T instance = null;
		try {
			instance = (T) getEntityManager().find(getClassePersistente(), id);
		} catch (PersistenceException re) {
			re.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca no banco de dados!");
		}
		return instance;
	}

	public final void refresh(T objeto) {
		getEntityManager().refresh(objeto);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	protected final Class<T> getClassePersistente() {
		return classePersistente;
	}
}
