package daoGeral;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import daoBasicas.*;

public abstract class DAOFactory {
	private static EntityManager manager;
	private static final EntityManagerFactory factory;

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		factory = Persistence.createEntityManagerFactory("bodegaPostgreSQLelephantSQL");
		if (manager == null || !manager.isOpen()) {
			manager = factory.createEntityManager();
		}
	}

	public static PermissoesDAO getPermissoesDAO() {
		PermissoesDAO dao = new PermissoesDAO(manager);
		return dao;
	}

	public static RegraNegocioDAO getRegraNegocioDAO() {
		RegraNegocioDAO dao = new RegraNegocioDAO(manager);
		return dao;
	}

	public static ProdNotasXMLDAO getProdNotasXMLDAO() {
		ProdNotasXMLDAO dao = new ProdNotasXMLDAO(manager);
		return dao;
	}

	public static UnidadeMedidaDAO getUnidadeMedidaDAO() {
		UnidadeMedidaDAO dao = new UnidadeMedidaDAO(manager);
		return dao;
	}

	public static CategoriaProdutoDAO getCategoriaProdutoDAO() {
		CategoriaProdutoDAO dao = new CategoriaProdutoDAO(manager);
		return dao;
	}

	public static SubCategoriaProdDAO getSubCategoriaProdDAO() {
		SubCategoriaProdDAO dao = new SubCategoriaProdDAO(manager);
		return dao;
	}

	public static ClienteDAO getClienteDAO() {
		ClienteDAO dao = new ClienteDAO(manager);
		return dao;
	}

	public static FornecedoresDAO getFornecedoresDAO() {
		FornecedoresDAO dao = new FornecedoresDAO(manager);
		return dao;
	}

	public static ItensEntPratDAO getItensEntPratDAO() {
		ItensEntPratDAO dao = new ItensEntPratDAO(manager);
		return dao;
	}

	public static ItensPedidoDAO getItensPedidoDAO() {
		ItensPedidoDAO dao = new ItensPedidoDAO(manager);
		return dao;
	}

	public static ItensVendaDAO getItensVendaDAO() {
		ItensVendaDAO dao = new ItensVendaDAO(manager);
		return dao;
	}

	public static PedidosDAO getPedidosDAO() {
		PedidosDAO dao = new PedidosDAO(manager);
		return dao;
	}

	public static ProdutosDAO getProdutosDAO() {
		ProdutosDAO dao = new ProdutosDAO(manager);
		return dao;
	}

	public static VendasDAO getVendasDAO() {
		VendasDAO dao = new VendasDAO(manager);
		return dao;
	}

	public static UsuarioDAO getUsuarioDAO() {
		UsuarioDAO dao = new UsuarioDAO(manager);
		return dao;
	}

	public static UsuarioMobileDAO getUsuarioMobileDAO() {
		UsuarioMobileDAO dao = new UsuarioMobileDAO(manager);
		return dao;
	}
}
