package daoGeral;

import javax.persistence.EntityManager;

public interface IDAOGenerico<T> {

	void inserir(Object obj);

	T alterar(T obj);

	void remover(T obj);

	void refresh(T object);

	void setEntityManager(EntityManager entityManager);

	EntityManager getEntityManager();

}