package basicas.mobile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(schema = "sistema")
@XmlRootElement(name = "listPermissionsApp")
public class PermissoesApp {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean verEstoque;

	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean verFornecedores;

	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean verPedidos;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((PermissoesApp) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isVerEstoque() {
		return verEstoque;
	}

	public void setVerEstoque(boolean verEstoque) {
		this.verEstoque = verEstoque;
	}

	public boolean isVerFornecedores() {
		return verFornecedores;
	}

	public void setVerFornecedores(boolean verFornecedores) {
		this.verFornecedores = verFornecedores;
	}

	public boolean isVerPedidos() {
		return verPedidos;
	}

	public void setVerPedidos(boolean verPedidos) {
		this.verPedidos = verPedidos;
	}
}