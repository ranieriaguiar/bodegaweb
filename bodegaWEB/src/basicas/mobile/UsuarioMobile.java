package basicas.mobile;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(schema = "sistema", name = "Usuario")
@XmlRootElement(name = "listUser")
public class UsuarioMobile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 25, nullable = false, unique = true)
	private String login;

	@Column(length = 25, nullable = false)
	private String senha;

	@Transient
	private int statusCode;

	@OneToOne
	@Cascade(CascadeType.DELETE)
	@PrimaryKeyJoinColumn
	private PermissoesApp permissoesApp;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((UsuarioMobile) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public PermissoesApp getPermissoesApp() {
		return permissoesApp;
	}

	public void setPermissoesApp(PermissoesApp permissoesApp) {
		this.permissoesApp = permissoesApp;
	}
}