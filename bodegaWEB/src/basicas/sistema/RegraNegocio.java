package basicas.sistema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "sistema")
public class RegraNegocio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	// Define o id do produto vendido por gramas, recebe 0 se n�o tiver
	@Column(nullable = false, columnDefinition = "int default 0")
	private Integer pratoFrios;

	// Define se haver� movimenta��o de vendas ou n�o
	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean vendas;

	// Define se haver� movimenta��o de prateleira ou n�o
	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean prateleira;

	// Define o valor de venda do produto com base no pre�o de compra
	@Column(nullable = false, precision = 3, scale = 2, columnDefinition = "float default 1")
	private float porcentagemLucro;

	// Define o id do fornecedor generico quando precisar criar um pedido
	// ficticio
	@Column(nullable = false, columnDefinition = "int default 1")
	private int fornecedorGenerico;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((RegraNegocio) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPratoFrios() {
		return pratoFrios;
	}

	public void setPratoFrios(Integer pratoFrios) {
		this.pratoFrios = pratoFrios;
	}

	public boolean isVendas() {
		return vendas;
	}

	public void setVendas(boolean vendas) {
		this.vendas = vendas;
	}

	public boolean isPrateleira() {
		return prateleira;
	}

	public void setPrateleira(boolean prateleira) {
		this.prateleira = prateleira;
	}

	public float getPorcentagemLucro() {
		return porcentagemLucro;
	}

	public void setPorcentagemLucro(float porcentagemLucro) {
		this.porcentagemLucro = porcentagemLucro;
	}

	public int getFornecedorGenerico() {
		return fornecedorGenerico;
	}

	public void setFornecedorGenerico(int fornecedorGenerico) {
		this.fornecedorGenerico = fornecedorGenerico;
	}

}
