package basicas.sistema;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "sistema")
public class Permissoes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private boolean alterar;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUsuario", insertable = true, updatable = true, nullable = false)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPagina", insertable = true, updatable = true, nullable = false)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private Paginas pagina;

	@Transient
	private boolean vendasAbertas = false;

	@Transient
	private boolean vendasFechadas = false;

	@Transient
	private boolean pedidosAbertos = false;

	@Transient
	private boolean pedidosFechados = false;

	@Transient
	private boolean fornecedores = false;

	@Transient
	private boolean enviarXML = false;

	@Transient
	private boolean produtos = false;

	@Transient
	private boolean movimentacao = false;

	@Transient
	private boolean relatorios = false;

	@Transient
	private boolean graficos = false;

	@Transient
	private String vendasAbertasUrl;

	@Transient
	private String vendasFechadasUrl;

	@Transient
	private String pedidosAbertosUrl;

	@Transient
	private String pedidosFechadosUrl;

	@Transient
	private String fornecedoresUrl;

	@Transient
	private String enviarXMLUrl;

	@Transient
	private String produtosUrl;

	@Transient
	private String movimentacaoUrl;

	@Transient
	private String relatoriosUrl;

	@Transient
	private String graficosUrl;

	public Permissoes converterPermissoes(List<Permissoes> listaPermissoes) {

		for (Permissoes p : listaPermissoes) {

			switch (p.getPagina().getNome()) {
			case "vendas":
				vendasAbertas = true;
				vendasAbertasUrl = p.getPagina().getUrl();
				break;
			case "vendasFechadas":
				vendasFechadas = true;
				vendasFechadasUrl = p.getPagina().getUrl();
				break;
			case "pedidos":
				pedidosAbertos = true;
				pedidosAbertosUrl = p.getPagina().getUrl();
				break;
			case "pedidosFechados":
				pedidosFechados = true;
				pedidosFechadosUrl = p.getPagina().getUrl();
				break;
			case "fornecedores":
				fornecedores = true;
				fornecedoresUrl = p.getPagina().getUrl();
				break;
			case "enviarXML":
				enviarXML = true;
				enviarXMLUrl = p.getPagina().getUrl();
				break;
			case "produtos":
				produtos = true;
				produtosUrl = p.getPagina().getUrl();
				break;
			case "movimentacao":
				movimentacao = true;
				movimentacaoUrl = p.getPagina().getUrl();
				break;
			case "relatorios":
				relatorios = true;
				relatoriosUrl = p.getPagina().getUrl();
				break;
			case "graficos":
				graficos = true;
				graficosUrl = p.getPagina().getUrl();
				break;
			}
		}
		return this;
	}

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((Permissoes) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Paginas getPagina() {
		return pagina;
	}

	public void setPagina(Paginas pagina) {
		this.pagina = pagina;
	}

	public boolean isAlterar() {
		return alterar;
	}

	public void setAlterar(boolean alterar) {
		this.alterar = alterar;
	}

	@XmlTransient
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isVendasAbertas() {
		return vendasAbertas;
	}

	public void setVendasAbertas(boolean vendasAbertas) {
		this.vendasAbertas = vendasAbertas;
	}

	public boolean isVendasFechadas() {
		return vendasFechadas;
	}

	public void setVendasFechadas(boolean vendasFechadas) {
		this.vendasFechadas = vendasFechadas;
	}

	public boolean isPedidosAbertos() {
		return pedidosAbertos;
	}

	public void setPedidosAbertos(boolean pedidosAbertos) {
		this.pedidosAbertos = pedidosAbertos;
	}

	public boolean isPedidosFechados() {
		return pedidosFechados;
	}

	public void setPedidosFechados(boolean pedidosFechados) {
		this.pedidosFechados = pedidosFechados;
	}

	public boolean isFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(boolean fornecedores) {
		this.fornecedores = fornecedores;
	}

	public boolean isEnviarXML() {
		return enviarXML;
	}

	public void setEnviarXML(boolean enviarXML) {
		this.enviarXML = enviarXML;
	}

	public boolean isProdutos() {
		return produtos;
	}

	public void setProdutos(boolean produtos) {
		this.produtos = produtos;
	}

	public boolean isMovimentacao() {
		return movimentacao;
	}

	public void setMovimentacao(boolean movimentacao) {
		this.movimentacao = movimentacao;
	}

	public boolean isRelatorios() {
		return relatorios;
	}

	public void setRelatorios(boolean relatorios) {
		this.relatorios = relatorios;
	}

	public boolean isGraficos() {
		return graficos;
	}

	public void setGraficos(boolean graficos) {
		this.graficos = graficos;
	}

	public String getVendasAbertasUrl() {
		return vendasAbertasUrl;
	}

	public void setVendasAbertasUrl(String vendasAbertasUrl) {
		this.vendasAbertasUrl = vendasAbertasUrl;
	}

	public String getVendasFechadasUrl() {
		return vendasFechadasUrl;
	}

	public void setVendasFechadasUrl(String vendasFechadasUrl) {
		this.vendasFechadasUrl = vendasFechadasUrl;
	}

	public String getPedidosAbertosUrl() {
		return pedidosAbertosUrl;
	}

	public void setPedidosAbertosUrl(String pedidosAbertosUrl) {
		this.pedidosAbertosUrl = pedidosAbertosUrl;
	}

	public String getPedidosFechadosUrl() {
		return pedidosFechadosUrl;
	}

	public void setPedidosFechadosUrl(String pedidosFechadosUrl) {
		this.pedidosFechadosUrl = pedidosFechadosUrl;
	}

	public String getFornecedoresUrl() {
		return fornecedoresUrl;
	}

	public void setFornecedoresUrl(String fornecedoresUrl) {
		this.fornecedoresUrl = fornecedoresUrl;
	}

	public String getEnviarXMLUrl() {
		return enviarXMLUrl;
	}

	public void setEnviarXMLUrl(String enviarXMLUrl) {
		this.enviarXMLUrl = enviarXMLUrl;
	}

	public String getProdutosUrl() {
		return produtosUrl;
	}

	public void setProdutosUrl(String produtosUrl) {
		this.produtosUrl = produtosUrl;
	}

	public String getMovimentacaoUrl() {
		return movimentacaoUrl;
	}

	public void setMovimentacaoUrl(String movimentacaoUrl) {
		this.movimentacaoUrl = movimentacaoUrl;
	}

	public String getRelatoriosUrl() {
		return relatoriosUrl;
	}

	public void setRelatoriosUrl(String relatoriosUrl) {
		this.relatoriosUrl = relatoriosUrl;
	}

	public String getGraficosUrl() {
		return graficosUrl;
	}

	public void setGraficosUrl(String graficosUrl) {
		this.graficosUrl = graficosUrl;
	}
}
