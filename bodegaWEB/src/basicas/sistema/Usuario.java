package basicas.sistema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "sistema")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int loginOk = 1;
	public static final int userAndPasswordDoesntMatch = 2;
	public static final int userDoesntExist = 3;
	public static final int userAlreadyExists = 4;
	public static final int serverError = 5;
	public static final int updateOk = 6;
	public static final int deleteOk = 7;

	public Usuario() {
		listaPermissoes = new ArrayList<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 25, nullable = false, unique = true)
	private String login;

	@Column(length = 25, nullable = false)
	private String senha;

	@Transient
	private int statusCode;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idHomePage", nullable = false)
	@Fetch(FetchMode.JOIN)
	private Paginas homePage;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idClient", nullable = false)
	@Fetch(FetchMode.JOIN)
	private Client client;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	private List<Permissoes> listaPermissoes;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((Usuario) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Permissoes> getListaPermissoes() {
		return listaPermissoes;
	}

	public void setListaPermissoes(List<Permissoes> listaPermissoes) {
		this.listaPermissoes = listaPermissoes;
	}

	public Paginas getHomePage() {
		return homePage;
	}

	public void setHomePage(Paginas homePage) {
		this.homePage = homePage;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}