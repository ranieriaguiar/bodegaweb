package basicas.negocio;

//  @author Ranieri
import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(schema = "negocio")
@AssociationOverrides({
		@AssociationOverride(name = "chaveComposta.produto", joinColumns = @JoinColumn(name = "idProduto")),
		@AssociationOverride(name = "chaveComposta.venda", joinColumns = @JoinColumn(name = "idVenda")) })
public class ItensVenda implements Serializable {
	private static final long serialVersionUID = 1L;

	public ItensVenda() {
		chaveComposta = new ItensVendaPK();
	}

	@EmbeddedId
	private ItensVendaPK chaveComposta;

	@Column(nullable = false)
	private int qtd;

	@Column(nullable = false, precision = 6, scale = 2)
	private float precoUnit;

	@Transient
	private float precoTotal;

	@Column(nullable = false, precision = 3, scale = 2)
	private float desconto;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && chaveComposta != null)
			return chaveComposta.equals(((ItensVenda) other).chaveComposta);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (chaveComposta != null) ? (getClass().hashCode() + chaveComposta.hashCode()) : super.hashCode();
	}

	public ItensVendaPK getChaveComposta() {
		return chaveComposta;
	}

	public void setChaveComposta(ItensVendaPK chaveComposta) {
		this.chaveComposta = chaveComposta;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	}

	public float getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(float precoUnit) {
		this.precoUnit = precoUnit;
	}

	public float getPrecoTotal() {
		return (precoUnit * desconto) * qtd;
	}

	public void setPrecoTotal(float precoTotal) {
		this.precoTotal = precoTotal;
	}

	public float getDesconto() {
		return desconto;
	}

	public void setDesconto(float desconto) {
		this.desconto = desconto;
	}
}
