package basicas.negocio;

//  @author Ranieri
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import basicas.sistema.Usuario;

@Entity
@Table(schema = "negocio")
@XmlRootElement(name = "listaOrder")
public class Pedidos implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int notaNormal = 1;
	public static final int notaXML = 2;
	public static final int notaSimulada = 3;

	public Pedidos() {
		listaItensPedido = new ArrayList<>();
		fornecedor = new Fornecedores();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date dataPedido;

	@Temporal(TemporalType.DATE)
	private Date dataEntrega;

	@Temporal(TemporalType.DATE)
	private Date dataVencimento;

	@Column(nullable = false)
	private boolean aberto;

	@Column(nullable = false, columnDefinition = "int default 1")
	private int tipoNota;

	@Column(nullable = false, precision = 3, scale = 2)
	private float porcentagem;

	@Transient
	private float totalPedido;

	@Transient
	private float totalNota;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idFornecedor", nullable = false)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private Fornecedores fornecedor;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUsuario", columnDefinition = "int default 1")
	@Fetch(FetchMode.JOIN)
	private Usuario usuario;

	@OneToMany(mappedBy = "chaveComposta.pedido", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<ItensPedido> listaItensPedido;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((Pedidos) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Fornecedores getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedores fornecedor) {
		this.fornecedor = fornecedor;
	}

	@XmlTransient
	public List<ItensPedido> getListaItensPedido() {
		return listaItensPedido;
	}

	public void setListaItensPedido(List<ItensPedido> listaItensPedido) {
		this.listaItensPedido = listaItensPedido;
	}

	public float getTotalPedido() {
		totalPedido = 0;
		if (listaItensPedido.size() != 0) {
			for (ItensPedido item : listaItensPedido) {
				totalPedido += item.getPrecoTotal();
			}
		}
		return totalPedido * porcentagem;
	}

	public void setTotalPedido(float totalPedido) {
		this.totalPedido = totalPedido;
	}

	public boolean isAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public float getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(float porcentagem) {
		this.porcentagem = porcentagem;
	}

	public float getTotalNota() {
		return totalNota;
	}

	public void setTotalNota(float totalNota) {
		this.totalNota = totalNota;
	}

	public int getTipoNota() {
		return tipoNota;
	}

	public void setTipoNota(int tipoNota) {
		this.tipoNota = tipoNota;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}