package basicas.negocio;

//  @author Ranieri
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "negocio")
@XmlRootElement(name = "listaCategorias")
public class CategoriaProduto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 15, nullable = false, unique = true)
	private String nome;

	@Column(nullable = false)
	private int ordem;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idSubCategoriaProd", insertable = true, updatable = true, nullable = true)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private SubCategoriaProd subCategoriaProd;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((CategoriaProduto) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public SubCategoriaProd getSubCategoriaProd() {
		return subCategoriaProd;
	}

	public void setSubCategoriaProd(SubCategoriaProd subCategoriaProd) {
		this.subCategoriaProd = subCategoriaProd;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
}
