package basicas.negocio;

//  @author Ranieri
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ItensPedidoPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Products produto;

	@ManyToOne
	private Pedidos pedido;

	public Products getProduto() {
		return produto;
	}

	public void setProduto(Products produto) {
		this.produto = produto;
	}

	public Pedidos getPedido() {
		return pedido;
	}

	public void setPedido(Pedidos pedido) {
		this.pedido = pedido;
	}
}
