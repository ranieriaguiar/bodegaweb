package basicas.negocio;

import java.io.Serializable;
//  @author Ranieri
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "negocio")
public class Vendas implements Serializable {
	private static final long serialVersionUID = 1L;

	public Vendas() {
		listaItensVenda = new ArrayList<ItensVenda>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaAbertura;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataAbertura;

	@Temporal(TemporalType.TIME)
	private Date horaFechamento;

	@Temporal(TemporalType.DATE)
	private Date dataFechamento;

	@Column(nullable = false)
	private int tipoPag;

	@Transient
	private float totalVenda;

	@Column(nullable = false)
	private boolean aberto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCliente", insertable = true, updatable = true)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private Clientes cliente;

	@OneToMany(mappedBy = "chaveComposta.venda", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<ItensVenda> listaItensVenda;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((Vendas) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getTipoPag() {
		return tipoPag;
	}

	public void setTipoPag(int tipoPag) {
		this.tipoPag = tipoPag;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public List<ItensVenda> getListaItensVenda() {
		return listaItensVenda;
	}

	public void setListaItensVenda(List<ItensVenda> listaItensVenda) {
		this.listaItensVenda = listaItensVenda;
	}

	public Date getHoraAbertura() {
		return horaAbertura;
	}

	public void setHoraAbertura(Date horaAbertura) {
		this.horaAbertura = horaAbertura;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getHoraFechamento() {
		return horaFechamento;
	}

	public void setHoraFechamento(Date horaFechamento) {
		this.horaFechamento = horaFechamento;
	}

	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}

	public boolean getAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public float getTotalVenda() {
		totalVenda = 0;
		if (listaItensVenda.size() != 0) {
			for (ItensVenda item : listaItensVenda) {
				totalVenda += item.getPrecoTotal();
			}
		}
		return totalVenda;
	}

	public void setTotalVenda(float totalVenda) {
		this.totalVenda = totalVenda;
	}
}
