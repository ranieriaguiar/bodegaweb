package basicas.negocio;

//  @author Ranieri
import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "negocio")
@XmlRootElement(name = "listaOrderItems")
@AssociationOverrides({
		@AssociationOverride(name = "chaveComposta.produto", joinColumns = @JoinColumn(name = "idProduto")),
		@AssociationOverride(name = "chaveComposta.pedido", joinColumns = @JoinColumn(name = "idPedido")) })
public class ItensPedido implements Serializable {
	private static final long serialVersionUID = 1L;

	public ItensPedido() {
		unidMedida = new UnidadeMedida();
		chaveComposta = new ItensPedidoPK();
	}

	@EmbeddedId
	private ItensPedidoPK chaveComposta;

	@Column(nullable = false, precision = 6, scale = 3)
	private float qtd;

	@Column(nullable = false, precision = 7, scale = 3)
	private float precoUnit;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUnidMedida", insertable = true, updatable = true)
	@Fetch(FetchMode.JOIN)
	private UnidadeMedida unidMedida;

	@Transient
	private float precoTotal;

	@Transient
	private String nomeProdXML;

	@Transient
	private String codProdXML;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && chaveComposta != null)
			return chaveComposta.equals(((ItensPedido) other).chaveComposta);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (chaveComposta != null) ? (getClass().hashCode() + chaveComposta.hashCode()) : super.hashCode();
	}

	public ItensPedidoPK getChaveComposta() {
		return chaveComposta;
	}

	public void setChaveComposta(ItensPedidoPK chaveComposta) {
		this.chaveComposta = chaveComposta;
	}

	public float getQtd() {
		return qtd;
	}

	public void setQtd(float qtd) {
		this.qtd = qtd;
	}

	public float getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(float precoUnit) {
		this.precoUnit = precoUnit;
	}

	public float getPrecoTotal() {
		return precoUnit * qtd;
	}

	public void setPrecoTotal(float precoTotal) {
		this.precoTotal = precoTotal;
	}

	public UnidadeMedida getUnidMedida() {
		return unidMedida;
	}

	public void setUnidMedida(UnidadeMedida unidMedida) {
		this.unidMedida = unidMedida;
	}

	public String getNomeProdXML() {
		return nomeProdXML;
	}

	public void setNomeProdXML(String nomeProdXML) {
		this.nomeProdXML = nomeProdXML;
	}

	public String getCodProdXML() {
		return codProdXML;
	}

	public void setCodProdXML(String codProdXML) {
		this.codProdXML = codProdXML;
	}
}
