package basicas.negocio;

//  @author Ranieri
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ItensVendaPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Products produto;

	@ManyToOne
	private Vendas venda;

	public Products getProduto() {
		return produto;
	}

	public void setProduto(Products produto) {
		this.produto = produto;
	}

	public Vendas getVenda() {
		return venda;
	}

	public void setVenda(Vendas venda) {
		this.venda = venda;
	}
}
