package basicas.negocio;

//  @author Ranieri
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceException;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "negocio", name = "Produtos")
@XmlRootElement(name = "listaProdutos")
@XmlAccessorType(XmlAccessType.FIELD)
public class Products implements Serializable {
	private static final long serialVersionUID = 1L;

	public Products() {
		categoria = new CategoriaProduto();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 45, nullable = false, unique = true, name = "nome")
	@XmlElement(name = "nome")
	private String name;

	@Column(nullable = false, precision = 6, scale = 2, name = "precoSugerido")
	@XmlElement(name = "precoSugerido")
	private float suggestedPrice;

	@Column(nullable = false, name = "estoque")
	@XmlElement(name = "estoque")
	private int stock;

	@Column(nullable = false, precision = 6, scale = 2, columnDefinition = "float default 0", name = "custoInicial")
	@XmlElement(name = "custoInicial")
	private float initialCost;

	@Column(nullable = false, columnDefinition = "int default 0", name = "estoqueInicial")
	@XmlElement(name = "estoqueInicial")
	private int initialStock;

	@Transient
	@XmlElement(name = "novoEstoque")
	private int newStock;

	@Transient
	@XmlTransient
	private float minimumQuantity;

	@Transient
	@XmlTransient
	private int input;

	@Transient
	@XmlTransient
	private int output;

	@Column(nullable = false, columnDefinition = "float default 0", precision = 5, scale = 2, name = "vendasPorDia")
	@XmlElement(name = "vendasPorDia")
	private float salesPerDay;

	@Column(nullable = false, columnDefinition = "int default 0", name = "diasProxEntrega")
	@XmlElement(name = "diasProxEntrega")
	private int nextDeliveryDays;

	@Column(nullable = false, columnDefinition = "bool default true", name = "ativado")
	@XmlElement(name = "ativado")
	private boolean activated;

	@Column(nullable = false)
	private Date version;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCategoriaProd", insertable = true, updatable = true)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private CategoriaProduto categoria;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((Products) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		try {
			StringBuffer res = new StringBuffer();
			String[] stringArray = name.toLowerCase().split(" ");

			for (String str : stringArray) {
				char[] charArray = str.trim().toCharArray();
				charArray[0] = Character.toUpperCase(charArray[0]);
				str = new String(charArray);

				res.append(str).append(" ");
			}
			this.name = res.toString().trim();

		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			throw new PersistenceException("Nome do produto n�o pode conter dois espa�os juntos!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao salvar o nome do produto!");
		}
	}

	public float getSuggestedPrice() {
		return suggestedPrice;
	}

	public void setSuggestedPrice(float suggestedPrice) {
		this.suggestedPrice = suggestedPrice;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public float getInitialCost() {
		return initialCost;
	}

	public void setInitialCost(float initialCost) {
		this.initialCost = initialCost;
	}

	public int getInitialStock() {
		return initialStock;
	}

	public void setInitialStock(int initialStock) {
		this.initialStock = initialStock;
	}

	public int getNewStock() {
		return newStock;
	}

	public void setNewStock(int newStock) {
		this.newStock = newStock;
	}

	public float getMinimumQuantity() {
		return salesPerDay * nextDeliveryDays;
	}

	public void setMinimumQuantity(float minimumQuantity) {
		this.minimumQuantity = minimumQuantity;
	}

	public int getInput() {
		return input;
	}

	public void setInput(int input) {
		this.input = input;
	}

	public int getOutput() {
		return output;
	}

	public void setOutput(int output) {
		this.output = output;
	}

	public float getSalesPerDay() {
		return salesPerDay;
	}

	public void setSalesPerDay(float salesPerDay) {
		this.salesPerDay = salesPerDay;
	}

	public int getNextDeliveryDays() {
		return nextDeliveryDays;
	}

	public void setNextDeliveryDays(int nextDeliveryDays) {
		this.nextDeliveryDays = nextDeliveryDays;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public CategoriaProduto getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaProduto categoria) {
		this.categoria = categoria;
	}
}
