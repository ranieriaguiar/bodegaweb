package basicas.negocio;

//  @author Ranieri
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import basicas.sistema.Usuario;

@Entity
@Table(schema = "negocio")
@XmlRootElement(name = "listaStockMovement")
public class ItEntradaPrateleira {

	public ItEntradaPrateleira() {
		unidMedida = new UnidadeMedida();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private int qtd;

	@Column(nullable = false)
	@Temporal(TemporalType.TIME)
	private Date hora;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date data;

	@Column(columnDefinition = "boolean default false")
	private boolean perda;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idProduto", nullable = false)
	@Fetch(FetchMode.JOIN)
	private Products produto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUnidMedida", nullable = false)
	@Fetch(FetchMode.JOIN)
	private UnidadeMedida unidMedida;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUsuario", columnDefinition = "int default 1")
	@Fetch(FetchMode.JOIN)
	private Usuario usuario;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((ItEntradaPrateleira) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Products getProduto() {
		return produto;
	}

	public void setProduto(Products produto) {
		this.produto = produto;
	}

	public UnidadeMedida getUnidMedida() {
		return unidMedida;
	}

	public void setUnidMedida(UnidadeMedida unidMedida) {
		this.unidMedida = unidMedida;
	}

	public boolean isPerda() {
		return perda;
	}

	public void setPerda(boolean perda) {
		this.perda = perda;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
