package basicas.negocio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(schema = "negocio")
public class ProdNotasXML {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 15, nullable = false)
	private String codProdXML;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idProduto", insertable = true, updatable = true)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
	private Products produto;

	@Override
	public boolean equals(Object other) {
		if (other != null && getClass() == other.getClass() && id != null)
			return id.equals(((ProdNotasXML) other).id);
		else
			return (other == this);
	}

	@Override
	public int hashCode() {
		return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodProdXML() {
		return codProdXML;
	}

	public void setCodProdXML(String codProdXML) {
		this.codProdXML = codProdXML;
	}

	public Products getProduto() {
		return produto;
	}

	public void setProduto(Products produto) {
		this.produto = produto;
	}
}