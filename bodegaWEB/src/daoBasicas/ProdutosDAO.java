package daoBasicas;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import java.util.Date;
import java.util.List;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.Products;
import daoGeral.DAOGenerico;

public class ProdutosDAO extends DAOGenerico<Products> {

	public ProdutosDAO(EntityManager em) {
		super(em);
	}

	public Products buscarPorNome(Products p) {
		try {
			String consulta = "SELECT p FROM Products p WHERE p.name = ?1";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			q1.setParameter(1, p.getName());
			return (Products) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do produto no banco de dados!");
		}
	}

	public List<Products> listarMovimentacoes() {
		try {
			String consulta = "SELECT p.id, p.name, totalDescido, p.suggestedPrice, (totalDescido * p.suggestedPrice) AS totalDinheiro "
					+ "FROM(" + "SELECT p.id, p.name, p.suggestedPrice, SUM(total) AS totalDescido " + "FROM("
					+ "SELECT p.id, p.name, p.suggestedPrice, (prat.qtd * unid.multiplicador) AS total "
					+ "FROM Products AS p " + "LEFT JOIN ItEntradaPrateleira AS prat ON p.id = prat.idproduto "
					+ "LEFT JOIN UnidadeMedida AS unid ON prat.idunidmedida = unid.id "
					+ "GROUP BY p.id, p.name, p.suggestedPrice, prat.qtd, unid.multiplicador) "
					+ "GROUP BY p.id, p.name, p.suggestedPrice) " + "ORDER BY totalDescido ASC;";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}

	public List<Products> listarMaisVendidos() {
		try {
			String consulta = "SELECT p FROM Products p WHERE p.activated = true ORDER BY p.salesPerDay DESC, p.categoria.ordem ASC, p.name ASC";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}

	// Mobile
	public List<Products> listar() {
		try {
			String consulta = "SELECT p FROM Products p ORDER BY p.categoria.ordem ASC, p.name ASC";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}

	// Mobile
	public List<Products> listar(Date data) {
		try {
			String consulta = "SELECT p FROM Products p WHERE p.version > ?1";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			q1.setParameter(1, data);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}

	public List<Products> listar(boolean ativado) {
		try {
			String consulta = "SELECT p FROM Products p WHERE p.activated = ?1 ORDER BY p.categoria.ordem ASC, p.name ASC";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			q1.setParameter(1, ativado);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}

	public List<Products> listar(CategoriaProduto categoria) {
		try {
			String consulta = "SELECT p FROM Products p WHERE p.categoria = ?1 ORDER BY p.id";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			q1.setParameter(1, categoria);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}

	public List<Products> listar(CategoriaProduto categoria, boolean aberta) {
		try {
			String consulta = "SELECT p FROM Products p WHERE p.activated = ?1 AND p.categoria = ?2 ORDER BY p.id";
			Query q1 = getEntityManager().createQuery(consulta, Products.class);
			q1.setParameter(1, aberta);
			q1.setParameter(2, categoria);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos produtos no banco de dados!");
		}
	}
}
