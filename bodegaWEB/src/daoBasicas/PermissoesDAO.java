package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.sistema.Permissoes;
import basicas.sistema.Usuario;
import daoGeral.DAOGenerico;

public class PermissoesDAO extends DAOGenerico<Permissoes> {

	public PermissoesDAO(EntityManager em) {
		super(em);
	}

	public List<Permissoes> listar(Usuario usuario) {
		try {
			String consulta = "SELECT p FROM Permissoes p WHERE p.usuario = ?1";
			Query q1 = getEntityManager().createQuery(consulta, Permissoes.class);
			q1.setParameter(1, usuario.getId());
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das permissoes no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das permissoes no banco de dados!");
		}
	}
}
