package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.CategoriaProduto;
import daoGeral.DAOGenerico;

public class CategoriaProdutoDAO extends DAOGenerico<CategoriaProduto> {

	public CategoriaProdutoDAO(EntityManager em) {
		super(em);
	}

	// Mobile
	public List<CategoriaProduto> listar() {
		try {
			String consulta = "SELECT c FROM CategoriaProduto c ORDER BY c.ordem";
			Query q1 = getEntityManager().createQuery(consulta, CategoriaProduto.class);
			return q1.getResultList();

		} catch (PersistenceException re) {
			re.printStackTrace();
			throw new PersistenceException(
					"Erro ocorreu durante a busca das categorias dos produtos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException(
					"Erro ocorreu durante a busca das categorias dos produtos no banco de dados!");
		}
	}
}
