package daoBasicas;

import javax.persistence.EntityManager;

import basicas.negocio.CatProdReceita;
import daoGeral.DAOGenerico;

public class CatProdReceitaDAO extends DAOGenerico<CatProdReceita> {

	public CatProdReceitaDAO(EntityManager em) {
		super(em);
	}
}
