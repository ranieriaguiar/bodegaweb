package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.Fornecedores;
import daoGeral.DAOGenerico;

public class FornecedoresDAO extends DAOGenerico<Fornecedores> {
	public FornecedoresDAO(EntityManager em) {
		super(em);
	}

	public Fornecedores buscarPorEmpresaXML(Fornecedores f) {
		try {
			String consulta = "SELECT f FROM Fornecedores f WHERE f.empresaXML = ?1";
			Query q1 = getEntityManager().createQuery(consulta, Fornecedores.class);
			q1.setParameter(1, f.getEmpresaXML());
			return (Fornecedores) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca da unidade de medida no banco de dados!");
		}
	}

	public Long contarPorEmpresa(Fornecedores f) {
		try {
			String consulta = "SELECT count(f) FROM Fornecedores f WHERE f.empresa = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, f.getEmpresa());
			return (Long) q1.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do fornecedor no banco de dados!");
		}
	}

	// Mobile
	public List<Fornecedores> listar() {
		try {
			String consulta = "SELECT f FROM Fornecedores f ORDER BY f.empresa";
			Query q1 = getEntityManager().createQuery(consulta, Fornecedores.class);
			return q1.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos fornecedores no banco de dados!");
		}
	}
}
