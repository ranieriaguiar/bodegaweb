package daoBasicas;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.mobile.UsuarioMobile;
import daoGeral.DAOGenerico;

public class UsuarioMobileDAO extends DAOGenerico<UsuarioMobile> {
	public UsuarioMobileDAO(EntityManager em) {
		super(em);
	}

	// Mobile
	public Long contarPorLogin(UsuarioMobile u) {
		try {
			String consulta = "SELECT count(u) FROM Usuario u WHERE u.login = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, u.getLogin());
			return (Long) q1.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do usu�rio no banco de dados!");
		}
	}

	// Mobile
	public UsuarioMobile buscarPorLoginSenha(UsuarioMobile u) {
		try {
			String consulta = "SELECT u FROM UsuarioMobile u WHERE u.login = ?1 AND u.senha = ?2";
			Query q1 = getEntityManager().createQuery(consulta, UsuarioMobile.class);
			q1.setParameter(1, u.getLogin());
			q1.setParameter(2, u.getSenha());
			return (UsuarioMobile) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
