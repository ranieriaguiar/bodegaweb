package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.ItensVenda;
import basicas.negocio.Products;
import basicas.negocio.Vendas;
import daoGeral.DAOGenerico;

public class ItensVendaDAO extends DAOGenerico<ItensVenda> {

	public ItensVendaDAO(EntityManager em) {
		super(em);
	}

	public int removerPorChaveComposta(ItensVenda i) {
		try {
			String consulta = "DELETE FROM ItensVenda i WHERE i.chaveComposta = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, i.getChaveComposta());
			return q1.executeUpdate();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o do item da venda no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o do item da venda no banco de dados!");
		}
	}

	public int removerPorVenda(Vendas v) {
		try {
			String consulta = "DELETE FROM ItensVenda i WHERE i.chaveComposta.venda = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, v.getId());
			return q1.executeUpdate();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o dos itens da venda no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o dos itens da venda no banco de dados!");
		}
	}

	// TODO M�todo errado, corrigir!!
	public float produtosVendidosPorDia(Products p) {
		try {
			int divisor = 100;
			String consulta = "SELECT SUM(i.qtd) FROM ItensVenda i WHERE i.chaveComposta.produto = ?1 ";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, p);
			q1.setMaxResults(divisor);
			return ((Long) q1.getSingleResult()).floatValue() / divisor;

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do produto no banco de dados!");
		}
	}

	public Long contarPorProdutos(Products p) {
		String consulta = "SELECT count(i) FROM ItensVenda i WHERE i.chaveComposta.produto = ?1";
		Query q1 = getEntityManager().createQuery(consulta);
		q1.setParameter(1, p);
		return (Long) q1.getSingleResult();
	}

	public List<ItensVenda> listar(Vendas v) {
		try {
			String consulta = "SELECT i FROM ItensVenda i WHERE i.chaveComposta.venda = ?1";
			Query q1 = getEntityManager().createQuery(consulta, ItensVenda.class);
			q1.setParameter(1, v);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos itens da venda no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos itens da venda no banco de dados!");
		}
	}
}
