package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.Fornecedores;
import basicas.negocio.Pedidos;
import daoGeral.DAOGenerico;

public class PedidosDAO extends DAOGenerico<Pedidos> {

	public PedidosDAO(EntityManager em) {
		super(em);
	}

	public Long contarPorId(Fornecedores f) {
		try {
			String consulta = "SELECT count(p) FROM Pedidos p WHERE fornecedor = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, f);
			return (Long) q1.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do fornecedor no banco de dados!");
		}
	}

	public Pedidos buscarUltimo(boolean aberta) {
		try {
			String consulta = "SELECT p FROM Pedidos p WHERE p.aberto = ?1 ORDER BY p.id DESC";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, aberta);
			q1.setMaxResults(1);
			return (Pedidos) q1.getSingleResult();
		} catch (NoResultException re) {
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do pedido no banco de dados!");
		}
	}

	// Mobile
	public List<Pedidos> listar(boolean aberta) {
		try {
			String consulta = "SELECT p FROM Pedidos p WHERE p.aberto = ?1 ORDER BY p.dataPedido DESC";
			Query q1 = getEntityManager().createQuery(consulta, Pedidos.class);
			q1.setParameter(1, aberta);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos pedidos no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos pedidos no banco de dados!");
		}
	}
}
