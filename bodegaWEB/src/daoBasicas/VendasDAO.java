package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.Vendas;
import daoGeral.DAOGenerico;

public class VendasDAO extends DAOGenerico<Vendas> {

	public VendasDAO(EntityManager em) {
		super(em);
	}

	public Vendas buscarUltima(boolean aberta) {
		try {
			String consulta = "SELECT v FROM Vendas v WHERE v.aberto = ?1 ORDER BY v.id DESC";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, aberta);
			q1.setMaxResults(1);
			return (Vendas) q1.getSingleResult();
		} catch (NoResultException re) {
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca da vendas no banco de dados!");
		}
	}

	public List<Vendas> listar(boolean aberta) {
		try {
			String consulta = "SELECT v FROM Vendas v WHERE v.aberto = ?1 ORDER BY v.id";
			Query q1 = getEntityManager().createQuery(consulta, Vendas.class);
			q1.setParameter(1, aberta);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das vendas no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das vendas no banco de dados!");
		}
	}
}
