package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.UnidadeMedida;
import daoGeral.DAOGenerico;

public class UnidadeMedidaDAO extends DAOGenerico<UnidadeMedida> {
	public UnidadeMedidaDAO(EntityManager em) {
		super(em);
	}

	public UnidadeMedida buscarPorNomeXML(UnidadeMedida u) {
		try {
			String consulta = "SELECT u FROM UnidadeMedida u WHERE u.nomeXML = ?1";
			Query q1 = getEntityManager().createQuery(consulta, UnidadeMedida.class);
			q1.setParameter(1, u.getNomeXML());
			return (UnidadeMedida) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca da unidade de medida no banco de dados!");
		}
	}

	// Mobile
	public List<UnidadeMedida> listar() {
		try {
			String consulta = "SELECT u FROM UnidadeMedida u ORDER BY u.ordem ASC";
			Query q1 = getEntityManager().createQuery(consulta, UnidadeMedida.class);
			return q1.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das unidades de medida no banco de dados!");
		}
	}
}
