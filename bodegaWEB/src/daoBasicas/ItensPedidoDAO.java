package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.ItensPedido;
import basicas.negocio.ItensPedidoPK;
import basicas.negocio.Pedidos;
import basicas.negocio.Products;
import daoGeral.DAOGenerico;

public class ItensPedidoDAO extends DAOGenerico<ItensPedido> {

	public ItensPedidoDAO(EntityManager em) {
		super(em);
	}

	public int removerPorChaveComposta(ItensPedido i) {
		try {
			String consulta = "DELETE FROM ItensPedido i WHERE i.chaveComposta = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, i.getChaveComposta());
			return q1.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o do item do pedido no banco de dados!");
		}
	}

	public int removerPorPedido(Pedidos p) {
		try {
			String consulta = "DELETE FROM ItensPedido i WHERE i.chaveComposta.pedido = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, p);
			return q1.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a remo��o dos itens do pedido no banco de dados!");
		}
	}

	public ItensPedido buscarPorChaveComposta(ItensPedidoPK i) {
		String consulta = "SELECT i FROM ItensPedido i WHERE i.chaveComposta = ?1";
		Query q1 = getEntityManager().createQuery(consulta);
		q1.setParameter(1, i);
		return (ItensPedido) q1.getSingleResult();
	}

	public ItensPedido buscarUltimoValor(Products p) {
		try {
			String consulta = "SELECT i FROM ItensPedido i WHERE i.chaveComposta.produto = ?1 AND i.precoUnit > 0 ORDER BY i.chaveComposta.pedido DESC";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, p);
			q1.setMaxResults(1);
			return (ItensPedido) q1.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos itens do pedido no banco de dados!");
		}
	}

	public Long contarPorProdutos(Products p) {
		String consulta = "SELECT count(i) FROM ItensPedido i WHERE i.chaveComposta.produto = ?1";
		Query q1 = getEntityManager().createQuery(consulta);
		q1.setParameter(1, p);
		return (Long) q1.getSingleResult();
	}

	// Mobile
	public List<ItensPedido> listar(Pedidos p) {
		try {
			String consulta = "SELECT i FROM ItensPedido i WHERE i.chaveComposta.pedido = ?1";
			Query q1 = getEntityManager().createQuery(consulta, ItensPedido.class);
			q1.setParameter(1, p);
			return q1.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos itens do pedido no banco de dados!");
		}
	}

	// Mobile
	public List<ItensPedido> listar() {
		try {
			String consulta = "SELECT i FROM ItensPedido i";
			Query q1 = getEntityManager().createQuery(consulta, ItensPedido.class);
			return q1.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca dos itens do pedido no banco de dados!");
		}
	}
}
