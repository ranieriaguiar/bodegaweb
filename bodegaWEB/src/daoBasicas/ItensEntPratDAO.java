package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.ItEntradaPrateleira;
import daoGeral.DAOGenerico;

public class ItensEntPratDAO extends DAOGenerico<ItEntradaPrateleira> {
	public ItensEntPratDAO(EntityManager em) {
		super(em);
	}

	public List<ItEntradaPrateleira> listar() {
		try {
			String consulta = "SELECT i FROM ItEntradaPrateleira i ORDER BY i.id DESC";
			Query q1 = getEntityManager().createQuery(consulta, ItEntradaPrateleira.class);
			return q1.getResultList();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das movimentações no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca das movimentações no banco de dados!");
		}
	}
}
