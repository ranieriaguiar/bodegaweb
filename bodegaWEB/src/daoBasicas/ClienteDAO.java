package daoBasicas;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.Clientes;
import daoGeral.DAOGenerico;

public class ClienteDAO extends DAOGenerico<Clientes> {
	public ClienteDAO(EntityManager em) {
		super(em);
	}

	public Clientes buscarPorNome(Clientes c) {
		try {
			String consulta = "SELECT c FROM Clientes c WHERE c.nome = ?1";
			Query q1 = getEntityManager().createQuery(consulta, Clientes.class);
			q1.setParameter(1, c.getNome());
			return (Clientes) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do cliente no banco de dados!");
		}
	}
}
