package daoBasicas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.SubCategoriaProd;
import daoGeral.DAOGenerico;

public class SubCategoriaProdDAO extends DAOGenerico<SubCategoriaProd> {

	private final String mensagemErro = "Erro ocorreu durante a busca das subCategorias dos produtos no banco de dados!";

	public SubCategoriaProdDAO(EntityManager em) {
		super(em);
	}

	// Mobile
	public List<SubCategoriaProd> listar() {
		try {
			String consulta = "SELECT s FROM SubCategoriaProd s";
			Query q1 = getEntityManager().createQuery(consulta, SubCategoriaProd.class);
			return q1.getResultList();

		} catch (PersistenceException re) {
			re.printStackTrace();
			throw new PersistenceException(mensagemErro);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException(mensagemErro);
		}
	}

}
