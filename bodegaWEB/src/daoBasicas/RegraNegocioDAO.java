package daoBasicas;

import javax.persistence.EntityManager;

import basicas.sistema.RegraNegocio;
import daoGeral.DAOGenerico;

public class RegraNegocioDAO extends DAOGenerico<RegraNegocio> {

	public RegraNegocioDAO(EntityManager em) {
		super(em);
	}
}
