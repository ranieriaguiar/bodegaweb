package daoBasicas;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import basicas.negocio.ProdNotasXML;
import daoGeral.DAOGenerico;

public class ProdNotasXMLDAO extends DAOGenerico<ProdNotasXML> {
	public ProdNotasXMLDAO(EntityManager em) {
		super(em);
	}

	public ProdNotasXML buscarPorCodProd(String cod) {
		try {
			String consulta = "SELECT p FROM ProdNotasXML p WHERE p.codProdXML = ?1";
			Query q1 = getEntityManager().createQuery(consulta, ProdNotasXML.class);
			q1.setParameter(1, cod);
			return (ProdNotasXML) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do produto no banco de dados!");
		}
	}
}
