package controller;

import basicas.negocio.Clientes;
import basicas.negocio.ItensVenda;
import basicas.negocio.Products;
import basicas.negocio.Vendas;
import basicas.sistema.RegraNegocio;
import daoBasicas.ClienteDAO;
import daoBasicas.VendasDAO;
import daoGeral.DAOFactory;
import daoBasicas.ItensVendaDAO;
import daoBasicas.ProdutosDAO;
import daoBasicas.RegraNegocioDAO;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

public class VendaRN {

	// Vendas

	public void salvar(Vendas v) {
		VendasDAO vendasDAO = DAOFactory.getVendasDAO();
		ClienteDAO clientesDAO = DAOFactory.getClienteDAO();
		try {
			Clientes c = clientesDAO.buscarPorNome(v.getCliente());
			if (c == null) {
				clientesDAO.inserir(v.getCliente());
				c = clientesDAO.buscarPorNome(v.getCliente());
			}
			v.setCliente(c);

			v.setDataAbertura(new Date());
			v.setHoraAbertura(new Date());
			v.setAberto(true);
			vendasDAO.inserir(v);

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao salvar a venda!");
		}
	}

	public void fechar(Vendas v) {
		VendasDAO vendasDAO = DAOFactory.getVendasDAO();
		try {
			alterarEstoque(v.getListaItensVenda(), false);

			v.setDataFechamento(new Date());
			v.setHoraFechamento(new Date());
			v.setAberto(false);
			vendasDAO.alterar(v);

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao fechar a venda!");
		}
	}

	public void excluir(Vendas v) {
		VendasDAO vendasDAO = DAOFactory.getVendasDAO();
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		try {
			alterarEstoque(itVenDAO.listar(v), true);
			apagarItem(v);
			vendasDAO.remover(v);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao excluir a venda!");
		}
	}

	public Vendas buscarUltima(boolean aberta) {
		VendasDAO vendasDAO = DAOFactory.getVendasDAO();
		return vendasDAO.buscarUltima(aberta);
	}

	public List<Vendas> listarVendas(boolean aberta) {
		VendasDAO vendasDAO = DAOFactory.getVendasDAO();
		return vendasDAO.listar(aberta);
	}

	private void alterarEstoque(List<ItensVenda> listaItensVenda, boolean incrementar) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		RegraNegocioDAO regrasDAO = DAOFactory.getRegraNegocioDAO();
		RegraNegocio regras = regrasDAO.buscarPorChave(1);
		Products novoProd;
		int quantidade;

		if (listaItensVenda != null && listaItensVenda.size() > 0 && regras.isVendas()) {

			for (ItensVenda item : listaItensVenda) {
				novoProd = item.getChaveComposta().getProduto();

				if (incrementar) {
					quantidade = novoProd.getStock() + item.getQtd();
				} else {
					quantidade = novoProd.getStock() - item.getQtd();

					// TODO M�todo errado, corrigir!!
					novoProd.setSalesPerDay(itVenDAO.produtosVendidosPorDia(novoProd));
				}

				novoProd.setStock(quantidade);
				produtosDAO.alterar(novoProd);
			}
		}
	}

	// Itens da Venda

	public void salvarItens(ItensVenda itVen) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		RegraNegocioDAO regrasDAO = DAOFactory.getRegraNegocioDAO();
		RegraNegocio regras = regrasDAO.buscarPorChave(1);
		try {
			Products prod = itVen.getChaveComposta().getProduto();
			ItensVenda item = itVenDAO.buscarPorChave(itVen.getChaveComposta());
			itVen.setDesconto(1);
			itVen.setPrecoUnit(produtosDAO.buscarPorChave(prod.getId()).getSuggestedPrice());

			if (item == null) {
				if (prod.getId() != regras.getPratoFrios()) {
					itVen.setQtd(1);
				}
				itVenDAO.inserir(itVen);
			} else {
				if (!prod.getId().equals(regras.getPratoFrios())) {
					itVen.setQtd(item.getQtd() + 1);
				} else {
					itVen.setQtd(item.getQtd() + itVen.getQtd());
				}
				itVenDAO.alterar(itVen);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao salvar o item da venda!");
		}
	}

	public void apagarItem(ItensVenda itVen) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		if (itVenDAO.removerPorChaveComposta(itVen) != 1) {
			throw new PersistenceException("Erro ao apagar o item da venda!");
		}
	}

	public void apagarItem(Vendas ven) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		if (itVenDAO.removerPorVenda(ven) < 1) {
			throw new PersistenceException("N�o tem itens da venda para apagar!");
		}
	}

	public void aumentarItem(ItensVenda itVen) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		int quantidade = itVen.getQtd();
		itVen.setQtd(quantidade + 1);
		itVenDAO.alterar(itVen);
	}

	public void diminuirItem(ItensVenda itVen) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		int quantidade = itVen.getQtd();
		itVen.setQtd(quantidade - 1);
		itVenDAO.alterar(itVen);
	}

	public List<ItensVenda> listaItens(Vendas v) {
		ItensVendaDAO itVenDAO = DAOFactory.getItensVendaDAO();
		return itVenDAO.listar(v);
	}
}
