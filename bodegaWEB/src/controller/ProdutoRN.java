package controller;

import basicas.mobile.UsuarioMobile;
import basicas.negocio.CategoriaProduto;
import basicas.negocio.Fornecedores;
import basicas.negocio.ItEntradaPrateleira;
import basicas.negocio.ItensPedido;
import basicas.negocio.Pedidos;
import basicas.negocio.Products;
import basicas.negocio.SubCategoriaProd;
import basicas.negocio.UnidadeMedida;
import basicas.sistema.RegraNegocio;
import basicas.sistema.Usuario;
import daoBasicas.CategoriaProdutoDAO;
import daoBasicas.ItensEntPratDAO;
import daoBasicas.ItensPedidoDAO;
import daoBasicas.ItensVendaDAO;
import daoBasicas.ProdutosDAO;
import daoBasicas.RegraNegocioDAO;
import daoBasicas.SubCategoriaProdDAO;
import daoBasicas.UnidadeMedidaDAO;
import daoBasicas.UsuarioDAO;
import daoGeral.DAOFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

public class ProdutoRN {

	public void salvar(Products p) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		if (produtosDAO.buscarPorNome(p) != null) {
			throw new PersistenceException("J� existe um produto com esse nome!");
		}
		p.setActivated(true);
		produtosDAO.inserir(p);
	}

	public void alterar(Products p) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		produtosDAO.alterar(p);
	}

	// Mobile
	public void alterar(List<Products> listaProdutos) {
		List<ItensPedido> listaItens = new ArrayList<>();
		ItensPedido itemPedido;
		Products produto;
		int estoque;
		ItEntradaPrateleira itemEntPrat;
		UnidadeMedida unidMedida = new UnidadeMedida();
		unidMedida.setId(1);

		for (Products p : listaProdutos) {
			produto = selecionar(p);

			if (produto != null) {
				estoque = produto.getStock() - p.getNewStock();
			} else {
				continue;
			}
			produto.setName(p.getName());
			produto.setSuggestedPrice(p.getSuggestedPrice());
			alterar(produto);

			if (estoque > 0) {
				itemEntPrat = new ItEntradaPrateleira();
				itemEntPrat.setUnidMedida(unidMedida);
				itemEntPrat.setProduto(produto);
				itemEntPrat.setPerda(true);
				itemEntPrat.setQtd(estoque);
				salvar(itemEntPrat);
			}
			if (estoque < 0) {
				itemPedido = new ItensPedido();
				itemPedido.getChaveComposta().setProduto(produto);
				itemPedido.setQtd(Math.abs(estoque));
				itemPedido.setUnidMedida(unidMedida);
				listaItens.add(itemPedido);
			}
		}

		if (!listaItens.isEmpty()) {
			RegraNegocioDAO regrasDAO = DAOFactory.getRegraNegocioDAO();
			RegraNegocio regras = regrasDAO.buscarPorChave(1);
			PedidoRN pedidoRN = new PedidoRN();
			Pedidos pedido = new Pedidos();
			Fornecedores fornecedor = new Fornecedores();

			fornecedor.setId(regras.getFornecedorGenerico());
			pedido.setDataVencimento(new Date());
			pedido.setDataEntrega(new Date());
			pedido.setDataPedido(new Date());
			pedido.setFornecedor(fornecedor);
			pedido.setTipoNota(Pedidos.notaSimulada);
			pedidoRN.salvarPedido(pedido);

			pedidoRN.salvarItemPedido(listaItens, pedido);

			pedidoRN.fecharPedido(pedido);
		}
	}

	public void excluir(Products p) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		ItensPedidoDAO itensPedDAO = DAOFactory.getItensPedidoDAO();
		ItensVendaDAO itensVenDAO = DAOFactory.getItensVendaDAO();
		if (itensPedDAO.contarPorProdutos(p) == 0L) {
			if (itensVenDAO.contarPorProdutos(p) == 0L) {
				produtosDAO.remover(p);
			} else {
				p.setActivated(false);
				produtosDAO.alterar(p);
			}
		} else {
			p.setActivated(false);
			produtosDAO.alterar(p);
		}
	}

	// Mobile
	public List<Products> listar() {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		return produtosDAO.listar();
	}

	public List<Products> listarMaisVendidos() {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		return produtosDAO.listarMaisVendidos();
	}

	public List<Products> listar(boolean ativado) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		return produtosDAO.listar(ativado);
	}

	// Mobile
	public List<Products> listar(Date data, UsuarioMobile user) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		UsuarioRN usuarioRN = new UsuarioRN();

		if (user != null) {
			user = usuarioRN.verificarLogin(user);

			if (user.getStatusCode() == Usuario.loginOk) {
				return produtosDAO.listar(data);
			}
		}
		return null;
	}

	// Mobile
	public List<Products> listar(UsuarioMobile user) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		UsuarioRN usuarioRN = new UsuarioRN();

		if (user != null) {
			user = usuarioRN.verificarLogin(user);

			if (user.getStatusCode() == Usuario.loginOk) {
				return produtosDAO.listar();
			}
		}
		return null;
	}

	public List<Products> listar(CategoriaProduto categoria) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		return produtosDAO.listar(categoria);
	}

	public List<Products> listar(CategoriaProduto categoria, boolean ativado) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		return produtosDAO.listar(categoria, ativado);
	}

	// Mobile
	public Products selecionar(Products p) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		return produtosDAO.buscarPorChave(p.getId());
	}

	// Categorias

	// Mobile
	public List<CategoriaProduto> listarCategorias() {
		CategoriaProdutoDAO categoriasDAO = DAOFactory.getCategoriaProdutoDAO();
		return categoriasDAO.listar();
	}

	// Mobile
	public List<SubCategoriaProd> listarSubCategorias() {
		SubCategoriaProdDAO subCategoriaProdDAO = DAOFactory.getSubCategoriaProdDAO();
		return subCategoriaProdDAO.listar();
	}

	// Prateleira

	public List<ItEntradaPrateleira> listarPrateleira() {
		ItensEntPratDAO itensEntPratDAO = DAOFactory.getItensEntPratDAO();
		return itensEntPratDAO.listar();
	}

	// Mobile
	public void salvar(ItEntradaPrateleira item) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		ItensEntPratDAO itensEntPratDAO = DAOFactory.getItensEntPratDAO();
		UnidadeMedidaDAO unidMedidaDAO = DAOFactory.getUnidadeMedidaDAO();
		try {
			Products novoProd;
			UnidadeMedida unidMed;
			int quantidade;

			if (item.getData() == null || new Date().before(item.getData())) {
				item.setData(new Date());
			}
			if (item.getHora() == null || new Date().before(item.getHora())) {
				item.setHora(new Date());
			}

			novoProd = new Products();
			novoProd = produtosDAO.buscarPorChave(item.getProduto().getId());
			unidMed = new UnidadeMedida();
			unidMed = unidMedidaDAO.buscarPorChave(item.getUnidMedida().getId());
			item.setProduto(novoProd);
			item.setUnidMedida(unidMed);

			itensEntPratDAO.inserir(item);

			quantidade = novoProd.getStock() - (item.getQtd() * unidMed.getMultiplicador());
			novoProd.setStock(quantidade);
			produtosDAO.alterar(novoProd);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao inserir a movimenta��o!");
		}
	}

	// Mobile
	public void salvar(List<ItEntradaPrateleira> listPrateleira) {
		UsuarioDAO usuarioDAO = DAOFactory.getUsuarioDAO();
		Usuario u;
		try {
			for (ItEntradaPrateleira item : listPrateleira) {
				u = usuarioDAO.buscarPorChave(item.getUsuario().getId());
				if (u != null) {
					item.setUsuario(u);
					salvar(item);
				} else {
					continue;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
