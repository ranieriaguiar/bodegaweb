package controller;

import daoBasicas.ProdutosDAO;
import daoGeral.DAOFactory;
import reports.ProdutosReport;

public class RelatorioRN {

	public void gerarEstoque() {
		ProdutosDAO produtoDAO = DAOFactory.getProdutosDAO();
		ProdutosReport produtoRep = new ProdutosReport();
		try {
			produtoRep.imprimir(produtoDAO.listar(), ProdutosReport.simples);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void gerarMovimentacoes() {
		ProdutosDAO produtoDAO = DAOFactory.getProdutosDAO();
		ProdutosReport produtoRep = new ProdutosReport();
		try {
			produtoRep.imprimir(produtoDAO.listarMovimentacoes(), ProdutosReport.simples);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
