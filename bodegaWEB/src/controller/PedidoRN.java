package controller;

import basicas.negocio.Fornecedores;
import basicas.negocio.ItensPedido;
import basicas.negocio.Pedidos;
import basicas.negocio.ProdNotasXML;
import basicas.negocio.Products;
import basicas.negocio.UnidadeMedida;
import basicas.sistema.RegraNegocio;
import daoBasicas.PedidosDAO;
import daoBasicas.ProdNotasXMLDAO;
import daoBasicas.ProdutosDAO;
import daoBasicas.RegraNegocioDAO;
import daoBasicas.UnidadeMedidaDAO;
import daoBasicas.FornecedoresDAO;
import daoBasicas.ItensPedidoDAO;

import daoGeral.DAOFactory;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

public class PedidoRN {

	// Pedidos

	// Mobile
	public void salvarPedido(Pedidos p) {
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		p.setPorcentagem(1);
		p.setAberto(true);
		pedidosDAO.inserir(p);
	}

	// Mobile
	public void fecharPedido(Pedidos p) {
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		try {
			// long variacao;
			// long porcent;
			//
			// if (p.getTotalPedido() == p.getTotalNota()) {
			// p.setPorcentagem(1);
			// } else {
			// if (p.getTotalPedido() < p.getTotalNota()) {
			// variacao = (long) (p.getTotalNota() - p.getTotalPedido());
			// porcent = (long) (variacao / (p.getTotalPedido() / 100));
			// p.setPorcentagem(1 + (porcent / 100));
			// } else {
			// variacao = (long) (p.getTotalPedido() - p.getTotalNota());
			// porcent = (long) (variacao / (p.getTotalPedido() / 100));
			// p.setPorcentagem(1 - (porcent / 100));
			// }
			// }

			List<ItensPedido> lista = itPedDAO.listar(p);

			if(lista.size() > 0) {
				alterarEstoque(lista, true);
				alterarPreco(lista);
				if (p.getDataEntrega() == null) {
					p.setDataEntrega(new Date());
				}
				if (p.getDataVencimento() == null) {
					p.setDataVencimento(new Date());
				}
				if (p.getDataPedido() == null) {
					p.setDataPedido(new Date());
				}
				p.setAberto(false);
				pedidosDAO.alterar(p);
			} else {
				throw new PersistenceException("Pedido sem itens!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao fechar o pedido!");
		}
	}

	public void excluirPedido(Pedidos p) {
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		try {
			alterarEstoque(itPedDAO.listar(p), false);
			apagarItem(p);
			pedidosDAO.remover(p);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao excluir o pedido!");
		}
	}

	public Pedidos buscarUltimo(boolean aberto) {
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		return pedidosDAO.buscarUltimo(aberto);
	}

	// Mobile
	public List<Pedidos> listarPedidos(boolean aberta) {
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		return pedidosDAO.listar(aberta);
	}

	private void alterarPreco(List<ItensPedido> lista) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		UnidadeMedidaDAO unidMedidaDAO = DAOFactory.getUnidadeMedidaDAO();
		RegraNegocioDAO regrasDAO = DAOFactory.getRegraNegocioDAO();
		RegraNegocio regras = regrasDAO.buscarPorChave(1);
		if (lista.size() > 0) {
			Products novoProd;
			UnidadeMedida unidMed;
			float valor;

			for (ItensPedido item : lista) {
				novoProd = item.getChaveComposta().getProduto();
				if (novoProd.getSuggestedPrice() == 0) {
					unidMed = unidMedidaDAO.buscarPorChave(item.getUnidMedida().getId());
					valor = (item.getPrecoUnit() / unidMed.getMultiplicador()) * regras.getPorcentagemLucro();

					novoProd.setSuggestedPrice(valor);
					novoProd.setInitialCost(valor);
					produtosDAO.alterar(novoProd);
					continue;
				}
				if (novoProd.getInitialCost() == 0) {
					unidMed = unidMedidaDAO.buscarPorChave(item.getUnidMedida().getId());
					valor = (item.getPrecoUnit() / unidMed.getMultiplicador()) * regras.getPorcentagemLucro();

					novoProd.setInitialCost(valor);
					produtosDAO.alterar(novoProd);
				}
			}
		}
	}

	private void alterarEstoque(List<ItensPedido> listaItens, boolean somar) {
		ProdutosDAO produtosDAO = DAOFactory.getProdutosDAO();
		UnidadeMedidaDAO unidMedidaDAO = DAOFactory.getUnidadeMedidaDAO();
		if (listaItens.size() > 0) {
			Products novoProd;
			UnidadeMedida unidMed;
			int quantidade;
			int novoValor;

			for (ItensPedido item : listaItens) {
				novoProd = new Products();
				novoProd = item.getChaveComposta().getProduto();
				unidMed = new UnidadeMedida();
				unidMed = unidMedidaDAO.buscarPorChave(item.getUnidMedida().getId());
				novoValor = (int) item.getQtd() * unidMed.getMultiplicador();

				if (somar) {
					quantidade = novoValor + novoProd.getStock();
				} else {
					quantidade = novoValor - novoProd.getStock();
				}

				novoProd.setStock(quantidade);
				produtosDAO.alterar(novoProd);
			}
		}
	}

	// Itens do Pedido

	public void salvarItemPedido(ItensPedido itPed) {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		try {
			ItensPedido item = itPedDAO.buscarPorChave(itPed.getChaveComposta());
			if (itPed.getPrecoUnit() == 0) {
				itPed.setPrecoUnit(buscarUltimoValor(itPed.getChaveComposta().getProduto()));
			}
			if (item == null) {
				itPedDAO.inserir(itPed);
			} else {
				itPed.setQtd(item.getQtd() + itPed.getQtd());
				itPedDAO.alterar(itPed);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao salvar o item do pedido!");
		}
	}

	// Mobile
	public void salvarItemPedido(List<ItensPedido> lista, Pedidos p) {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		try {
			for (ItensPedido itensPedido : lista) {
				if (itensPedido.getPrecoUnit() == 0) {
					itensPedido.setPrecoUnit(buscarUltimoValor(itensPedido.getChaveComposta().getProduto()));
				}
				itensPedido.getChaveComposta().setPedido(p);
				itPedDAO.inserir(itensPedido);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao salvar o item do pedido!");
		}
	}

	private float buscarUltimoValor(Products p) {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		ItensPedido item = itPedDAO.buscarUltimoValor(p);
		if (item != null) {
			return item.getPrecoUnit() / item.getUnidMedida().getMultiplicador();
		}
		return 0;
	}

	public void apagarItem(ItensPedido itPed) {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		if (itPedDAO.removerPorChaveComposta(itPed) != 1) {
			throw new PersistenceException("Erro ao apagar o item do pedido!");
		}
	}

	public void apagarItem(Pedidos ped) {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		if (itPedDAO.removerPorPedido(ped) < 1) {
			throw new PersistenceException("N�o tem itens do pedido para apagar!");
		}
	}

	public List<ItensPedido> listaItensPedido(Pedidos p) {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		return itPedDAO.listar(p);
	}

	// Mobile
	public List<ItensPedido> listaItensPedido() {
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		return itPedDAO.listar();
	}

	// Mobile
	public List<UnidadeMedida> listarUnidMedida() {
		UnidadeMedidaDAO unidMedidaDAO = DAOFactory.getUnidadeMedidaDAO();
		return unidMedidaDAO.listar();
	}

	// Fornecedores

	public void salvarFornecedor(Fornecedores f) {
		FornecedoresDAO fornecedoresDAO = DAOFactory.getFornecedoresDAO();
		if (fornecedoresDAO.contarPorEmpresa(f) > 0L) {
			throw new PersistenceException("J� existe uma empresa com esse nome!");
		}
		f.setAtivado(true);
		fornecedoresDAO.inserir(f);
	}

	public void alterarFornecedor(Fornecedores f) {
		FornecedoresDAO fornecedoresDAO = DAOFactory.getFornecedoresDAO();
		fornecedoresDAO.alterar(f);
	}

	public void excluirFornecedor(Fornecedores f) {
		FornecedoresDAO fornecedoresDAO = DAOFactory.getFornecedoresDAO();
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		if (pedidosDAO.contarPorId(f) > 0L) {
			f.setAtivado(false);
			fornecedoresDAO.alterar(f);
		} else {
			fornecedoresDAO.remover(f);
		}
	}

	// Mobile
	public List<Fornecedores> listFornecedores() {
		FornecedoresDAO fornecedoresDAO = DAOFactory.getFornecedoresDAO();
		return fornecedoresDAO.listar();
	}

	// Produtos das Notas XML

	public void salvarNotaXML(Pedidos p) {
		PedidosDAO pedidosDAO = DAOFactory.getPedidosDAO();
		FornecedoresDAO fornecedoresDAO = DAOFactory.getFornecedoresDAO();
		ItensPedidoDAO itPedDAO = DAOFactory.getItensPedidoDAO();
		ProdNotasXMLDAO prodNotasXMLDAO = DAOFactory.getProdNotasXMLDAO();
		try {
			ProdNotasXML prodXML;

			// Procura fornecedor por XML, se n�o existir ele cria
			Fornecedores forne = fornecedoresDAO.buscarPorEmpresaXML(p.getFornecedor());
			if (forne == null) {
				forne = new Fornecedores();

				forne.setEmpresa(p.getFornecedor().getEmpresaXML());
				forne.setEmpresaXML(p.getFornecedor().getEmpresaXML());
				forne.setFone(p.getFornecedor().getFone());
				forne.setAtivado(true);
				fornecedoresDAO.inserir(forne);
			}
			p.setFornecedor(forne);

			for (ItensPedido item : p.getListaItensPedido()) {

				// Verifica se algum item n�o foi vinculado a um produto
				if (item.getChaveComposta().getProduto().getId() == null) {
					throw new PersistenceException("N�o foi selecionado um produto ao item!");
				}

				// Procura alguma rela��o de cod XML com produto, se n�o existir
				// ele cria
				prodXML = prodNotasXMLDAO.buscarPorCodProd(item.getCodProdXML());
				if (prodXML == null) {
					prodXML = new ProdNotasXML();
					prodXML.setCodProdXML(item.getCodProdXML());
					prodXML.setProduto(item.getChaveComposta().getProduto());
					prodNotasXMLDAO.inserir(prodXML);
				}
			}

			pedidosDAO.inserir(p);
			itPedDAO.inserirColecao(p.getListaItensPedido());
			alterarEstoque(p.getListaItensPedido(), true);

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ao salvar nota!");
		}
	}

	public Pedidos verificarProdNotasXML(Pedidos p) {
		FornecedoresDAO fornecedoresDAO = DAOFactory.getFornecedoresDAO();
		UnidadeMedidaDAO unidMedidaDAO = DAOFactory.getUnidadeMedidaDAO();
		ProdNotasXMLDAO prodNotasXMLDAO = DAOFactory.getProdNotasXMLDAO();
		ProdNotasXML prodXML;
		UnidadeMedida unid;
		Fornecedores forne;

		forne = fornecedoresDAO.buscarPorEmpresaXML(p.getFornecedor());
		if (forne != null) {
			p.setFornecedor(forne);
		}

		for (ItensPedido item : p.getListaItensPedido()) {
			// Buscar os produtos
			prodXML = prodNotasXMLDAO.buscarPorCodProd(item.getCodProdXML());

			if (prodXML != null) {
				item.getChaveComposta().setProduto(prodXML.getProduto());
			}

			// Buscar as unidades de medida
			unid = unidMedidaDAO.buscarPorNomeXML(item.getUnidMedida());
			if (unid != null) {
				item.setUnidMedida(unid);
			}

		}
		return p;
	}
}