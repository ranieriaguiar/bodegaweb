package controller;

import basicas.mobile.UsuarioMobile;
import basicas.sistema.RegraNegocio;
import basicas.sistema.Usuario;
import daoBasicas.RegraNegocioDAO;
import daoBasicas.UsuarioDAO;
import daoBasicas.UsuarioMobileDAO;
import daoGeral.DAOFactory;

public class UsuarioRN {

	public Usuario verificarLogin(Usuario u) {
		UsuarioDAO usuarioDAO = DAOFactory.getUsuarioDAO();
		if (usuarioDAO.contarPorLogin(u) > 0) {
			Usuario usuario = usuarioDAO.buscarPorLoginSenha(u);

			if (usuario != null) {
				usuario.setStatusCode(Usuario.loginOk);
				return usuario;
			} else {
				u.setStatusCode(Usuario.userAndPasswordDoesntMatch);
				return u;
			}

		} else {
			u.setStatusCode(Usuario.userDoesntExist);
			return u;
		}
	}

	// Mobile
	public UsuarioMobile verificarLogin(UsuarioMobile u) {
		UsuarioMobileDAO usuarioDAO = DAOFactory.getUsuarioMobileDAO();
		if (usuarioDAO.contarPorLogin(u) > 0) {
			UsuarioMobile usuario = usuarioDAO.buscarPorLoginSenha(u);

			if (usuario != null) {
				usuario.setStatusCode(Usuario.loginOk);
				return usuario;
			} else {
				u.setStatusCode(Usuario.userAndPasswordDoesntMatch);
				return u;
			}

		} else {
			u.setStatusCode(Usuario.userDoesntExist);
			return u;
		}
	}

	public RegraNegocio buscarRegras() {
		RegraNegocioDAO regraDAO = DAOFactory.getRegraNegocioDAO();
		return regraDAO.buscarPorChave(1);
	}
}