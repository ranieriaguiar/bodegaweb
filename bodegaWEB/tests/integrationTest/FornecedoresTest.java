package integrationTest;

import javax.persistence.PersistenceException;

import org.junit.Test;

import basicas.negocio.Fornecedores;
import facade.Fachada;

public class FornecedoresTest {

	Fachada fachada = new Fachada();
	RandomString genString = new RandomString(8);

	@Test
	public void salvar() {
		Fornecedores f = new Fornecedores();
		f.setEmpresa(genString.nextString());
		f.setNome(genString.nextString());
		fachada.save(f);
	}

	@Test(expected = PersistenceException.class)
	public void salvarComEmpresaExtenso() {
		Fornecedores f = new Fornecedores();
		f.setEmpresa(new RandomString(51).nextString());
		f.setNome(genString.nextString());
		fachada.save(f);
	}

	@Test(expected = PersistenceException.class)
	public void salvarComNomeExtenso() {
		Fornecedores f = new Fornecedores();
		f.setEmpresa(genString.nextString());
		f.setNome(new RandomString(26).nextString());
		fachada.save(f);
	}

	@Test(expected = PersistenceException.class)
	public void salvarComFoneExtenso() {
		Fornecedores f = new Fornecedores();
		f.setEmpresa(genString.nextString());
		f.setNome(genString.nextString());
		f.setFone(new RandomString(13).nextString());
		fachada.save(f);
	}

	@Test(expected = IllegalStateException.class)
	public void salvarComEmpresaDuplicada() {
		Fornecedores f = new Fornecedores();
		f.setEmpresa("teste");
		f.setNome(genString.nextString());
		fachada.save(f);
	}

	@Test(expected = PersistenceException.class)
	public void salvarSemEmpresa() {
		Fornecedores f = new Fornecedores();
		f.setNome(genString.nextString());
		fachada.save(f);
	}

	@Test(expected = PersistenceException.class)
	public void salvarSemNome() {
		Fornecedores f = new Fornecedores();
		f.setEmpresa(genString.nextString());
		fachada.save(f);
	}

	//@Test
	public void delete() {
		Fornecedores f = new Fornecedores();
		f.setId(1);
		fachada.delete(f);
	}

	@Test
	public void update() {
		Fornecedores f = new Fornecedores();
		f.setId(1);
		f.setEmpresa(genString.nextString());
		f.setNome(genString.nextString());
		fachada.update(f);
	}	
}