package integrationTest;

import javax.persistence.PersistenceException;

import org.junit.Test;

import basicas.negocio.CategoriaProduto;
import basicas.negocio.Products;
import facade.Fachada;

public class ProdutosTest {

	Fachada fachada = new Fachada();
	RandomString genString = new RandomString(8);

	@Test
	public void salvar() {
		Products p = new Products();
		CategoriaProduto cat = new CategoriaProduto();

		p.setName(genString.nextString());
		cat.setId(1);
		p.setCategoria(cat);
		fachada.save(p);
	}

	@Test(expected = PersistenceException.class)
	public void salvarComNomeExtenso() {
		Products p = new Products();
		CategoriaProduto cat = new CategoriaProduto();

		p.setName(new RandomString(46).nextString());
		cat.setId(1);
		p.setCategoria(cat);
		fachada.save(p);
	}

	@Test(expected = PersistenceException.class)
	public void salvarComNomeDuplicado() {
		Products p = new Products();
		CategoriaProduto cat = new CategoriaProduto();

		p.setName("teste");
		cat.setId(1);
		p.setCategoria(cat);
		fachada.save(p);
	}

	@Test(expected = PersistenceException.class)
	public void salvarSemNome() {
		Products p = new Products();
		CategoriaProduto cat = new CategoriaProduto();
		cat.setId(1);
		p.setCategoria(cat);
		fachada.save(p);
	}

	@Test(expected = PersistenceException.class)
	public void salvarSemCategoria() {
		Products p = new Products();
		p.setName(genString.nextString());
		fachada.save(p);
	}

	@Test
	public void update() {
		Products p = new Products();
		CategoriaProduto cat = new CategoriaProduto();

		p.setName(genString.nextString());
		cat.setId(1);
		p.setCategoria(cat);
		fachada.update(p);
	}
}
