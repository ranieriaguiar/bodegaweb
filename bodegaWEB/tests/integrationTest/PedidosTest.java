package integrationTest;

import javax.persistence.PersistenceException;

import org.junit.Test;

import basicas.negocio.Fornecedores;
import basicas.negocio.ItensPedido;
import basicas.negocio.Pedidos;
import basicas.negocio.Products;
import basicas.negocio.UnidadeMedida;
import facade.Fachada;

public class PedidosTest {

	Fachada fachada = new Fachada();

	@Test
	public void salvar() {
		Fornecedores f = new Fornecedores();
		f.setId(1);

		Pedidos p = new Pedidos();
		p.setFornecedor(f);
		fachada.save(p);
	}

	@Test
	public void fechar() {
		Pedidos ped = novoPedido();
		Products prod = new Products();
		ItensPedido itPed = new ItensPedido();
		UnidadeMedida un = new UnidadeMedida();

		prod.setId(1);
		un.setId(1);
		itPed.setUnidMedida(un);
		itPed.getChaveComposta().setProduto(prod);
		itPed.getChaveComposta().setPedido(ped);
		fachada.saveItem(itPed);

		fachada.close(ped);
		fachada.delete(ped);
	}

	@Test(expected = IllegalStateException.class)
	public void fecharPedidoComItemSemUnidMedida() {
		Pedidos ped = novoPedido();
		Products prod = new Products();
		ItensPedido itPed = new ItensPedido();

		prod.setId(1);
		itPed.getChaveComposta().setProduto(prod);
		itPed.getChaveComposta().setPedido(ped);
		fachada.saveItem(itPed);

		fachada.close(ped);
		fachada.delete(ped);
	}

	@Test(expected = IllegalStateException.class)
	public void fecharPedidoSemItens() {
		Pedidos p = novoPedido();
		fachada.close(p);
		fachada.delete(p);
	}

	@Test(expected = PersistenceException.class)
	public void fecharPedidoInexistente() {
		Pedidos p = new Pedidos();
		p.setId(500);
		fachada.close(p);
		fachada.delete(p);
	}

	@Test(expected = PersistenceException.class)
	public void fecharPedidoNulo() {
		Pedidos p = null;
		fachada.close(p);
		fachada.delete(p);
	}

	@Test(expected = IllegalStateException.class)
	public void salvarComFornecedorInexistente() {
		Fornecedores f = new Fornecedores();
		f.setId(50);

		Pedidos p = new Pedidos();
		p.setFornecedor(f);
		fachada.save(p);
	}

	@Test(expected = IllegalStateException.class)
	public void salvarSemFornecedor() {
		Pedidos p = new Pedidos();
		fachada.save(p);
	}

	private Pedidos novoPedido() {
		Fornecedores f = new Fornecedores();
		f.setId(1);

		Pedidos p = new Pedidos();
		p.setFornecedor(f);
		fachada.save(p);
		return fachada.buscarUltimo(true);
	}	
}