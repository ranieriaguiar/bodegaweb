package integrationTest;

import javax.persistence.PersistenceException;

import org.junit.Test;

import basicas.negocio.Clientes;
import basicas.negocio.ItensVenda;
import basicas.negocio.Vendas;
import basicas.negocio.Products;
import facade.Fachada;

public class VendasTest {

	Fachada fachada = new Fachada();
	RandomString genString = new RandomString(8);

	@Test
	public void salvar() {
		Clientes c = new Clientes();
		c.setNome(genString.nextString());
		c.setFone(genString.nextString());

		Vendas v = new Vendas();
		v.setCliente(c);
		fachada.save(v);
	}
	
	@Test(expected = PersistenceException.class)
	public void salvarVendaSemCliente() {
		Vendas v = new Vendas();
		fachada.save(v);
	}

	@Test
	public void fechar() {
		Vendas v = novaVenda();
		Products prod = new Products();
		ItensVenda itVen = new ItensVenda();

		prod.setId(1);
		itVen.getChaveComposta().setProduto(prod);
		itVen.getChaveComposta().setVenda(v);
		fachada.saveItem(itVen);

		fachada.close(v);
		fachada.delete(v);
	}

	@Test(expected = PersistenceException.class)
	public void fecharVendaSemProdutos() {
		Vendas v = novaVenda();
		fachada.close(v);
		fachada.delete(v);
	}

	@Test(expected = PersistenceException.class)
	public void fecharVendaInexistente() {
		Vendas v = new Vendas();
		v.setId(500);
		fachada.close(v);
		fachada.delete(v);
	}

	@Test(expected = PersistenceException.class)
	public void fecharVendaNula() {
		Vendas v = null;
		fachada.close(v);
		fachada.delete(v);
	}

	private Vendas novaVenda() {
		Clientes c = new Clientes();
		c.setNome("Ranieri");

		Vendas v = new Vendas();
		v.setCliente(c);
		fachada.save(v);
		return fachada.buscarUltima(true);
	}
}