package unitTest;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import basicas.negocio.Products;
import daoBasicas.ProdutosDAO;
import daoGeral.DAOFactory;

public class ProdutosDAOTest {

	@Mock
	private ProdutosDAO produtosDAOmock = DAOFactory.getProdutosDAO();

	@Before
	public void beforeTest(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void buscarProdutosPorNome() {
		Products prod = new Products();
		prod.setName("teste");

		Mockito.when(produtosDAOmock.buscarPorNome(prod)).thenReturn(prod);
		assertNotNull(produtosDAOmock.buscarPorNome(prod));
	}

	@Test
	public void listarProdutos() {
		//Mockito.when(produtosDAOmock.listar()).thenReturn(List<Products>);
		List<Products> listProd = produtosDAOmock.listar();
		assertNotNull(listProd);
	}

}
