package functionalTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class BaseFunctionalTests {

	protected static WebDriver driver;

	protected final String loginId = "formLogin:inputLogin";
	protected final String passwordId = "formLogin:inputSenha";
	protected final String loginButtonId = "formLogin:loginButton";

	@BeforeClass
	public static void setUp(){
		System.setProperty("webdriver.chrome.driver", "D:/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://node12873-bodega.br1.saphir.global/");
	}

	@AfterClass
	public static void tearDown(){
		driver.quit();
	}

	protected boolean loginValido() {
		sendValues(loginId, "Ranieri");
		sendValues(passwordId, "123456");
		clickButton(loginButtonId);
		delay(1);
		return driver.getTitle().contains("Bodega WEB - Estoque");
	}

	protected void delay(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void sendValues(String id, String sentValues) {
		WebElement element = driver.findElement(By.id(id));
		element.clear();
		element.sendKeys(sentValues);
	}

	protected void clickButton(String id) {
		WebElement element = driver.findElement(By.id(id));
		element.click();
	}
}