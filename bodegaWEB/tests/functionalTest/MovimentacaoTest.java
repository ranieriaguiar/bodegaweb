package functionalTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import integrationTest.RandomString;

public class MovimentacaoTest extends BaseFunctionalTests {

	private RandomString genString = new RandomString(8);

	private final String novoItemButtonId = "dataProd:tableItens:novoItemButton";
	private final String salvarItemButtonId = "dialogs:inserirItemButton";
	private final String inputProdNameId = "dialogs:dialogNovoItem:tableProdutos:inputProdName";
	private final String selectFirstItemId = "dialogs:dialogNovoItem:tableProdutos_row_0";

	@Before
	public void login() {
		Assert.assertTrue(loginValido());
	}

	@Test
	public void novaMovimentacao() {
		clickButton(novoItemButtonId);
		sendValues(inputProdNameId, genString.nextString());
		clickButton(selectFirstItemId);



		clickButton(salvarItemButtonId);
		delay(2);
	}

}
