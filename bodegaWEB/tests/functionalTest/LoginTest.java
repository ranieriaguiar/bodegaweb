package functionalTest;

import org.junit.Assert;
import org.junit.Test;

public class LoginTest extends BaseFunctionalTests{	

	@Test
	public void loginComUsuarioEmInvalido() {
		sendValues(loginId, "Ranieri1");
		sendValues(passwordId, "123457");
		clickButton(loginButtonId);

		boolean existeMensagem = driver.getPageSource().contains("Usu�rio n�o existe!");
		Assert.assertTrue(existeMensagem);
		delay(1);
	}

	@Test
	public void loginComSenhaEmInvalido() {
		sendValues(loginId, "Ranieri");
		sendValues(passwordId, "123457");
		clickButton(loginButtonId);

		boolean existeMensagem = driver.getPageSource().contains("Usu�rio e senha n�o coincidem!");
		Assert.assertTrue(existeMensagem);
		delay(1);
	}

	@Test
	public void loginComUsuarioEmBranco() {
		sendValues(loginId, "");
		sendValues(passwordId, "123457");
		clickButton(loginButtonId);

		boolean existeMensagem = driver.getPageSource().contains("Campo 'Login' � obrigat�rio!");
		Assert.assertTrue(existeMensagem);
		delay(1);
	}

	@Test
	public void loginComSenhaEmBranco() {
		sendValues(loginId, "Ranieri");
		sendValues(passwordId, "");
		clickButton(loginButtonId);

		boolean existeMensagem = driver.getPageSource().contains("Campo 'Senha' � obrigat�rio!");
		Assert.assertTrue(existeMensagem);
		delay(1);
	}
}